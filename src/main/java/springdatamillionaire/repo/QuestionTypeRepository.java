package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.QuestionType;

import java.util.List;

public interface QuestionTypeRepository extends CrudRepository<QuestionType, Long> {
    public List<QuestionType> findByKind(String kind);
}
