package springdatamillionaire.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.DTO.GameHistoryDTO;
import springdatamillionaire.DTO.QuestionDTO;
import springdatamillionaire.DTO.UserDTO;
import springdatamillionaire.entity.Question;
import springdatamillionaire.service.PaymentsService;
import springdatamillionaire.service.QuestionService;
import springdatamillionaire.service.UserService;

import javax.validation.Valid;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private UserService userService;
    private QuestionService questionService;
    private PaymentsService paymentsService;

    @Autowired
    public UserController(
            UserService userService,
            QuestionService questionService,
            PaymentsService paymentsService) {
        this.userService = userService;
        this.questionService = questionService;
        this.paymentsService = paymentsService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    String create(@RequestBody UserDTO user) {
        System.out.println("from POST: CREATE");
        userService.create(user);
        return "User successfully created";
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    List<UserDTO> findAll() {
        return userService.findall();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    UserDTO findById(@PathVariable long userId) {
        return userService.findById(userId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    UserDTO update(@RequestBody @Valid UserDTO userDTO, @PathVariable long userId) {
        return userService.update(userDTO,userId);
    }

    @RequestMapping( method = RequestMethod.DELETE, value = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    void delete(@PathVariable long userId) {
        userService.delete(userId);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    void deletAll() {
        userService.delete_all();
    }

    @GetMapping("/{userId}/room")
    @ResponseStatus(HttpStatus.OK)
    long userRoomId(@PathVariable long userId) {
        return userService.userRoomId(userId);
    }

    @GetMapping("/{userId}/games_history")
    @ResponseStatus(HttpStatus.OK)
    List<GameHistoryDTO> gamesHistory(@PathVariable long userId) {
        return paymentsService.history(userId);
    }
}
