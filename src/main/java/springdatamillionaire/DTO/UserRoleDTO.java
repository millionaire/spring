package springdatamillionaire.DTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class UserRoleDTO {

        public UserRoleDTO(long id,String name) {
            this.id = id;
            this.name = name;
        }

        public UserRoleDTO() {
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        private long id;
        @NotNull
        private String name;
    }




