package springdatamillionaire.DTO;

import java.util.List;

public class SavedAnswersDTO {
    private long userId;
    private List<SavedAnswerDTO> savedAnswerDTOs;

    public SavedAnswersDTO(long userId, List<SavedAnswerDTO> savedAnswerDTOs) {
        this.userId = userId;
        this.savedAnswerDTOs = savedAnswerDTOs;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<SavedAnswerDTO> getSavedAnswerDTOs() {
        return savedAnswerDTOs;
    }

    public void setSavedAnswerDTOs(List<SavedAnswerDTO> savedAnswerDTOs) {
        this.savedAnswerDTOs = savedAnswerDTOs;
    }
}
