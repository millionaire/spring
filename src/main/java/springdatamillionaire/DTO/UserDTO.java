package springdatamillionaire.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDTO {

    // variables
    private long id;
    private String name;
    private String userRoleName;
    private long userRoleId;
    private boolean isWinner;
    private boolean isCorrect;
    private String password;
    private double answerTime;
    private int money;

    public UserDTO(
            long id,
            String name,
            String userRoleName,
            long userRoleId,
            int money) {
        this.id = id;
        this.name = name;
        this.userRoleName = userRoleName;
        this.userRoleId = userRoleId;
        this.money = money;
    }

    public UserDTO(String username, String password) {
        this.name = username;
        this.password = password;
    }

    public UserDTO() {
    }

    // getters and setters

    public int getMoney() {
        return money;
    }
    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUserRoleName() {
        return userRoleName;
    }
    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }
    public long getUserRoleId() {
        return userRoleId;
    }
    public void setUserRoleId(long userRoleId) {
        this.userRoleId = userRoleId;
    }
    public boolean isWinner() {
        return isWinner;
    }
    public void setWinner(boolean winner) {
        isWinner = winner;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public double getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(double answerTime) {
        this.answerTime = answerTime;
    }
}
