package springdatamillionaire.DTO;

public class SavedAnswerDTO {
    private long questionId;
    private long answerId;
    private boolean isRight;
    private long answerTime;

    public SavedAnswerDTO(long questionId,long answerId, boolean isRight, long answerTime) {
        this.questionId = questionId;
        this.isRight = isRight;
        this.answerTime = answerTime;
        this.answerId = answerId;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public boolean getIsRight() {
        return isRight;
    }

    public void setIsRight(boolean isRight) {
        this.isRight = isRight;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }
}
