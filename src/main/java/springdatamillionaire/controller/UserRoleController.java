package springdatamillionaire.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.DTO.UserRoleDTO;
import springdatamillionaire.service.UserRoleService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user_roles")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    UserRoleDTO create(@RequestBody @Valid UserRoleDTO userRoleDTO) {
        return userRoleService.create(userRoleDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    List<UserRoleDTO> findAll() {
        return userRoleService.findall();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    void deleteAll() {
        userRoleService.delete_all();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    void delete(@PathVariable long id) {
         userRoleService.delete(id);
    }


}
