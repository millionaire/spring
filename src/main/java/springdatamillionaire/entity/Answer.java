package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Answer {
    public Answer(String text, boolean isRight, Question question) {
        this.text = text;
        this.isRight = isRight;
        this.question = question;
    }

    public Answer() {
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        this.isRight = right;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String text;

    private boolean isRight;

    @OneToOne
    private Question question;
}
