package springdatamillionaire.DTO;

public class GameHistoryDTO {
    long gameId;
    long roomId;
    int money;

    public GameHistoryDTO(long gameId, long roomId, int money) {
        this.gameId = gameId;
        this.roomId = roomId;
        this.money = money;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
