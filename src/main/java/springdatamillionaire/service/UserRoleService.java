package springdatamillionaire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.UserRoleDTO;
import springdatamillionaire.entity.UserRole;
import springdatamillionaire.repo.UserRoleRepository;

import java.util.ArrayList;
import java.util.List;
@Service
public class UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserRoleDTO create(UserRoleDTO userRoleDTO) {
        return toDTO(userRoleRepository.save(fromDTO(userRoleDTO)));

    }

    public List<UserRoleDTO> findall()
    {
        List<UserRole> userRoles = userRoleRepository.findAll();
        ArrayList<UserRoleDTO> userDTOS = new ArrayList<UserRoleDTO>();
        for (UserRole userRole : userRoles) {
            userDTOS.add(toDTO(userRole));
        }
        return userDTOS;
    }

    public void delete_all()
    {
        userRoleRepository.deleteAll();
    }

    public void delete(long id)
    {
        userRoleRepository.delete(id);
    }


    private UserRole fromDTO(UserRoleDTO userRoleDTO)
    {
        return new UserRole(userRoleDTO.getName());
    }

    private UserRoleDTO toDTO(UserRole user)
    {
        return new UserRoleDTO(user.getId(),user.getName());
    }
}
