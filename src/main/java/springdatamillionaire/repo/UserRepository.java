package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    void delete(User deleted);

    List<User> findAll();

    User findOne(Long id);

    List<User> findByName(String name);

    User save(User saved);

    List<User> findByRoom_Id(long roomid);
}
