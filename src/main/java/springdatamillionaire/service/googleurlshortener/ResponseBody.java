package springdatamillionaire.service.googleurlshortener;

public class ResponseBody {
    String kind;
    String id;
    String longUrl;

    public ResponseBody(String kind, String id, String longUrl) {
        this.kind = kind;
        this.id = id;
        this.longUrl = longUrl;
    }

    public ResponseBody() {
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }
}
