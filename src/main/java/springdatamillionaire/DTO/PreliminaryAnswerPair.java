package springdatamillionaire.DTO;

public class PreliminaryAnswerPair {
    private long answerId;
    private long ord;

    public PreliminaryAnswerPair(long answerId, long ord) {
        this.answerId = answerId;
        this.ord = ord;
    }

    public PreliminaryAnswerPair() {
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getOrd() {
        return ord;
    }

    public void setOrd(long ord) {
        this.ord = ord;
    }
}
