package springdatamillionaire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.AnswerDTO;
import springdatamillionaire.entity.Answer;
import springdatamillionaire.repo.AnswerRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnswerService {
    private AnswerRepository answerRepository;
    @Autowired
    public AnswerService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

     public void delete(long questionId){
        List<Answer> answers = answerRepository.findByQuestion_Id(questionId);
        for (Answer answer : answers) {
            answerRepository.delete(answer.getId());
        }
    }


    public List<AnswerDTO> findAnswersByQuestionId(long id) {
        ArrayList<AnswerDTO> answerDTOs = new ArrayList<AnswerDTO>();
        List<Answer> answers = answerRepository.findByQuestion_Id(id);
        for (Answer answer : answers) {
            answerDTOs.add(toDTO(answer));
        }
        return answerDTOs;
    }

    public long findCorrectAnswerIdByQuestionId(long id) {
        List<Answer> answers = answerRepository.findByQuestion_Id(id);
        for (Answer answer : answers) {
            if(answer.isRight()) {
                return answer.getId();
            }
        }
        return -1;
    }

    public boolean checkAnswerById(long answerId) {
        return answerRepository.findOne(answerId).isRight();
    }

    private AnswerDTO toDTO(Answer answer) {
        return new AnswerDTO(answer.getId(),answer.getText(), answer.isRight());
    }
}
