package springdatamillionaire.entity;

import javax.persistence.*;
import org.joda.time.*;

@Entity
public class SavedAnswer {
    public SavedAnswer(long answerId,boolean isRight, Room room, User user, Question question, long answerTime) {
        this.isRight = isRight;
        this.question = question;
        this.user = user;
        this.room = room;
        this.answerTime = answerTime;
        this.answerId = answerId;
    }

    public SavedAnswer() {
    }

    public long getId() {
        return id;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean right) {
        this.isRight = right;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long answerId;

    private boolean isRight;

    private long answerTime;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @OneToOne
    private Question question;

    @ManyToOne
    private User user;

    @ManyToOne
    private Room room;
}

