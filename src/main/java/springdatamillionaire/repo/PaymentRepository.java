package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Payment;
import springdatamillionaire.entity.Room;
import springdatamillionaire.entity.User;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {
    Payment findOneByRoomAndUser(Room room, User user);
    List<Payment> findAllByUser(User user);
}
