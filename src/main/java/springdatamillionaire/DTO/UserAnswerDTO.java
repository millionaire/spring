package springdatamillionaire.DTO;

import java.util.List;

public class UserAnswerDTO {
    private long answerId;
    private long correct;
    private List<PreliminaryAnswerPair> answerPairs;
    private List<PreliminaryAnswerPair> correctAnswerPairs;
    private List<UserDTO> prelimPlayers;
    private RoomDTO newRoomState;

    public UserAnswerDTO(long answerId, long correct, List<PreliminaryAnswerPair> correctAnswerPairs) {
        this.answerId = answerId;
        this.correct = correct;
        this.correctAnswerPairs = correctAnswerPairs;
    }

    public UserAnswerDTO(long answerId, long correct) {
        this.answerId = answerId;
        this.correct = correct;
    }

    public UserAnswerDTO(long correct) {
        this.correct = correct;
    }

    public UserAnswerDTO(List<PreliminaryAnswerPair> correctAnswerPairs, List<UserDTO> prelimPlayers) {
        this.correctAnswerPairs = correctAnswerPairs;
        this.prelimPlayers = prelimPlayers;
    }

    public UserAnswerDTO( long correct, RoomDTO newRoomState) {
        this.correct = correct;
        this.newRoomState = newRoomState;
    }

    public UserAnswerDTO() {
    }


    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public long getCorrect() {
        return correct;
    }

    public void setCorrect(long correct) {
        this.correct = correct;
    }

    public List<PreliminaryAnswerPair> getAnswerPairs() {
        return answerPairs;
    }

    public void setAnswerPairs(List<PreliminaryAnswerPair> answerPairs) {
        this.answerPairs = answerPairs;
    }

    public List<PreliminaryAnswerPair> getCorrectAnswerPairs() {
        return correctAnswerPairs;
    }

    public void setCorrectAnswerPairs(List<PreliminaryAnswerPair> correctAnswerPairs) {
        this.correctAnswerPairs = correctAnswerPairs;
    }

    public List<UserDTO> getPrelimPlayers() {
        return prelimPlayers;
    }

    public void setPrelimPlayers(List<UserDTO> prelimPlayers) {
        this.prelimPlayers = prelimPlayers;
    }

    public RoomDTO getNewRoomState() {
        return newRoomState;
    }

    public void setNewRoomState(RoomDTO newRoomState) {
        this.newRoomState = newRoomState;
    }
}
