package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import springdatamillionaire.entity.SavedAnswer;

import java.util.List;

public interface SavedAnswerRepository extends CrudRepository<SavedAnswer, Long> {
    public List<SavedAnswer> findByUser_IdAndRoom_Id(long userId,long roomId);
    public SavedAnswer findByUser_IdAndRoom_IdAndQuestion_Id(long userId,long roomId,long questionId);
    public List<SavedAnswer> findByRoom_IdAndQuestion_Id(long roomId,long questionId);
    @Transactional
    public List<SavedAnswer> removeByRoom_IdAndQuestion_Id(long roomId,long questionId);

    public List<SavedAnswer> findByRoom_Id(long roomId); //TODO groupby
}
