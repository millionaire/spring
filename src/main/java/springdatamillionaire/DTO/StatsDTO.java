package springdatamillionaire.DTO;

import javax.persistence.*;
import java.math.BigDecimal;

public class StatsDTO {
    String username;
    int correctAnswers;
    int money;
    boolean failed;

    public StatsDTO(String username, int correctAnswers, int money, boolean failed) {
        this.username = username;
        this.correctAnswers = correctAnswers;
        this.money = money;
        this.failed = failed;
    }

    public String getUsername() {
        return username;
    }

    public boolean isFailed() {
        return failed;
    }

    public int getMoney() {
        return money;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(int correctAnswers) {
        this.correctAnswers = correctAnswers;
    }
}
