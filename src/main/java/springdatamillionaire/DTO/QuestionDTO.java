package springdatamillionaire.DTO;

import java.util.List;

public class QuestionDTO {
    private long id;
    private String text;
    private boolean preliminary;
    //private String category;
    //private String questionType;
    private List<AnswerDTO> answerDTOs;
    private int ord;

    public QuestionDTO(long id, String text, List<AnswerDTO> answerDTOs, boolean preliminary, int ord) {
        this.id = id;
        this.text = text;
        this.answerDTOs = answerDTOs;
        this.preliminary = preliminary;
        this.ord = ord;
    }

    public QuestionDTO(){
        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isPreliminary() {
        return preliminary;
    }

    public void setPreliminary(boolean preliminary) {
        this.preliminary = preliminary;
    }

    public List<AnswerDTO> getAnswerDTOs() {
        return answerDTOs;
    }

    public void setAnswerDTOs(List<AnswerDTO> answerDTOs) {
        this.answerDTOs = answerDTOs;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }
}
