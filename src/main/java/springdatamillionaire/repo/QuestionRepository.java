package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Question;

import java.util.List;

public interface QuestionRepository extends CrudRepository<Question, Long> {
    // public Question findByRooms_IdAndCategory_Ord(long id,int ord);
    public List<Question> findByRemoved(boolean removed);
    public List<Question> findByCategory_OrdAndRemoved(int ord, boolean removed);
    public List<Question> findByQuestionType_KindAndRemoved(String kind, boolean removed);

}
