package springdatamillionaire.DTO;

import java.util.ArrayList;
import java.util.List;

public class HintPollDTO {

    private List<AnswerPercent> answers;

    public HintPollDTO(long answerid1,double percent1,
                       long answerid2,double percent2,
                       long answerid3,double percent3,
                       long answerid4,double percent4) {
        this.answers = new ArrayList<AnswerPercent>();
        answers.add(new AnswerPercent(answerid1,percent1));
        answers.add(new AnswerPercent(answerid2,percent2));
        answers.add(new AnswerPercent(answerid3,percent3));
        answers.add(new AnswerPercent(answerid4,percent4));
    }

    public HintPollDTO(List<AnswerPercent> answers) {
        this.answers = answers;
    }

    public List<AnswerPercent> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerPercent> answers) {
        this.answers = answers;
    }
}
