package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private Room room;

    @ManyToOne
    private User user;

    private int money;

    // constructors

    public Payment(Room room, User user, int money) {
        this.room = room;
        this.user = user;
        this.money = money;
    }

    public Payment() {
    }

    // getters and setters

    public long getId() {
        return id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
