package springdatamillionaire.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.DTO.*;
import springdatamillionaire.entity.Hint;
import springdatamillionaire.DTO.StatsDTO;
import springdatamillionaire.service.RoomService;
import springdatamillionaire.service.SavedAnswerService;
import springdatamillionaire.service.UserService;
import springdatamillionaire.service.googleurlshortener.GoogleUrlShortener;
import springdatamillionaire.service.security.SessionUser;
import springdatamillionaire.service.security.TokenAuthenticationService;
import springdatamillionaire.service.security.TokenHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/rooms")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RoomController {

    private RoomService roomService;
    private UserService userService;
    private TokenAuthenticationService authenticationService;
    private SavedAnswerService savedAnswerService;

    @Autowired
    public RoomController(
            RoomService roomService,
            UserService userService,
            TokenAuthenticationService authenticationService,
            SavedAnswerService savedAnswerService) {
        this.roomService = roomService;
        this.userService = userService;
        this.authenticationService = authenticationService;
        this.savedAnswerService = savedAnswerService;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    RoomDTO create(HttpServletRequest request){ return roomService.create(request.getHeader("access_token"));}

    @RequestMapping(value = "/{roomId}", method = RequestMethod.DELETE)
    void delete(@PathVariable long roomId,HttpServletRequest request) {
        roomService.delete(roomId,request.getHeader("access_token"));
    }

    @PostMapping("/{roomId}/join")
    @ResponseStatus(HttpStatus.OK)
    RoomDTO joinRoom(
            @PathVariable long roomId,
            HttpServletRequest request,
            @RequestBody Map<String, String> jsonBody) {
        long userId = Long.parseLong(
            TokenHandler.parseSessionUser(
                request.getHeader("access_token")).getUserId()
        );
        return roomService.joinRoom(roomId, userId);
    }



    @PostMapping("/{roomId}/start")
    @ResponseStatus(HttpStatus.OK)
    void startGame(
            @PathVariable long roomId,
            HttpServletRequest request) {
        String token = request.getHeader("access_token");
        long userId =  Long.parseLong(TokenHandler.parseSessionUser(token).getUserId());
        roomService.startGame(roomId, userId);
    }

    @PostMapping("/{roomId}/stop")
    @ResponseStatus(HttpStatus.OK)
    String stopGame(
            @PathVariable long roomId,
            HttpServletRequest request) {
        String token = request.getHeader("access_token");
        long userId =  Long.parseLong(TokenHandler.parseSessionUser(token).getUserId());
        return roomService.stopGame(roomId, userId);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    List<RoomDTO> findAll() {
        return roomService.findall();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{roomId}/question")
    @ResponseStatus(HttpStatus.OK)
    QuestionDTO getRoomQuestion(@PathVariable long roomId) {
        QuestionDTO questionDTO = roomService.getQuestionDTO(roomId);
        return questionDTO;
    }
    @RequestMapping(method = RequestMethod.PATCH, value = "/{roomId}")
    @ResponseStatus(HttpStatus.OK)
    void updateRoom(@PathVariable long roomId, @RequestBody RoomDTO roomDTO, HttpServletRequest request)  {
        String token = request.getHeader("access_token");
        long userId =  Long.parseLong(TokenHandler.parseSessionUser(token).getUserId());
        if(roomDTO.isContinuePrelim()){
            roomService.continuePrelim(roomId,userId);
        }
        else if (roomDTO.isContinueGame()) {
            roomService.continueGame(roomId,userId);
        }
        else if (roomDTO.isSkipLetsplay()){
            roomService.skipLetsplay(roomId,userId);
        }
        else if (roomDTO.isFinalAnswer()) {
            roomService.sendCorrectAnswer(roomId,userId);
        }
        else if (roomDTO.isAnotherPlayer()) {
            roomService.pickAnotherPlayer(roomId,userId);
        }
        else if (roomDTO.isStartHintCall()){
            roomService.startHintCall(roomId,userId);
        }
    }

    @GetMapping("/joinlink/{roomtoken}")
    @ResponseStatus(value = HttpStatus.OK)
    public long joinRoonByLink(@PathVariable String roomtoken, HttpServletResponse response) {
        long roomId = roomService.getRoomIdByLink(roomtoken);
        if (roomId != -1 ){
            SessionUser sessionUser =  userService.createTempUser();
            authenticationService.addAuthentication(response, sessionUser);
        }
        return roomId;
    }
    @GetMapping("{roomId}/joinlink")
    public String createRoomLink(@PathVariable long roomId,HttpServletRequest request){
        String token = request.getHeader("access_token");
        return roomService.generateLink(roomId,token);
    }

    @GetMapping("{roomId}/enable_invite")
    public String createInviteLink(@PathVariable long roomId, HttpServletRequest request, HttpServletResponse response) {
        String access_token = request.getHeader("access_token");
        long userId = Long.parseLong(TokenHandler.parseSessionUser(access_token).getUserId());

        if (roomService.checkInviteRights(roomId, userId)) { // only host can create invite link
            response.setStatus(200);

            String inviteToken = roomService.randomInviteToken();

            String longUrl = "https://ncmillionaire.ddns.net/invite/" + roomId + "/" + inviteToken;
//            String longUrl = "http://localhost:3000/invite/" + roomId + "/" + inviteToken;
            String shortUrl = GoogleUrlShortener.shorten(longUrl);

            roomService.saveInvite(roomId, inviteToken, shortUrl);

            return shortUrl;
        }
        else {
            response.setStatus(403);
            return "";
        }
    }

    @GetMapping("/invite/{roomId}/{inviteToken}")
    public UserDTO invite(
            @PathVariable long roomId,
            @PathVariable String inviteToken,
            HttpServletResponse response) {
        if (roomService.isRoomInvitable(roomId) && roomService.isInviteTokenValid(roomId, inviteToken)) {
            SessionUser tempUser = userService.createTempUser();
            long userId = Long.parseLong(tempUser.getUserId());
            roomService.joinRoom(roomId, userId);
            return new UserDTO(tempUser.getUsername(), tempUser.getPassword());
        }
        else {
            response.setStatus(403);
            return null;
        }
    }

    @GetMapping("{roomId}")
    public RoomDTO findOne(@PathVariable long roomId) {
        return roomService.findOne(roomId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{roomId}/answerid")
    @ResponseStatus(HttpStatus.OK)
    void answerToQuestion(@RequestBody UserAnswerDTO userAnswerDTO,@PathVariable long roomId
                                    ,HttpServletRequest request) {
        roomService.checkAndSaveAnswer(userAnswerDTO,roomId, request.getHeader("access_token"));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{roomId}/question/hint/5050")
    @ResponseStatus(HttpStatus.OK)
    void Hint5050(@PathVariable long roomId,HttpServletRequest request) {
        roomService.getHint(roomId,request.getHeader("access_token"),Hint.HINT5050);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{roomId}/question/hint/poll")
    @ResponseStatus(HttpStatus.OK)
    void HintPoll(@PathVariable long roomId,HttpServletRequest request) {
        roomService.getHint(roomId,request.getHeader("access_token"),Hint.HINTPOLL);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{roomId}/question/hint/call")
    @ResponseStatus(HttpStatus.OK)
    void HintCall(@PathVariable long roomId,HttpServletRequest request) {
        roomService.getHint(roomId,request.getHeader("access_token"),Hint.HINTCALL);
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/{roomId}/{userId}/savedAnswers")
//    @ResponseStatus(HttpStatus.OK)
//    SavedAnswersDTO savedAnswers(@PathVariable long roomId, @PathVariable long userId) {
//        return roomService.getSavedAnswers(roomId,userId);
//    }

    @GetMapping("/{roomId}/stats")
    @ResponseStatus(HttpStatus.OK)
//    List<StatsDTO> stats(@PathVariable long roomId) {
//        return savedAnswerService.stats(roomId);
//    }
    List<StatsDTO> stats(@PathVariable long roomId) {
        return roomService.stats(roomId);
    }
}
