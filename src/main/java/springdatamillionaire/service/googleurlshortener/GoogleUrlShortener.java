package springdatamillionaire.service.googleurlshortener;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public class GoogleUrlShortener {

    public static String shorten(String url) {
        String apiKey = "AIzaSyChGeLZZ_AAh4CQ1LKz_HHIOeuMLw6rcFA";

        HttpHeaders headers = new HttpHeaders();
        RequestBody body = new RequestBody(url);

        HttpEntity<RequestBody> request = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseBody response = restTemplate.postForObject(
                "https://www.googleapis.com/urlshortener/v1/url?key=" + apiKey,
                request,
                ResponseBody.class
        );

        return response.id;
    }
}
