package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "VoicePhrase")
@Table(name = "voice_phrase")
public class VoicePhrase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String name;

    @ManyToOne
    @JoinColumn(name = "action_id")
    private VoiceAction action;

    ///////////////////////////

    public VoicePhrase() {
    }

    public VoicePhrase(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VoiceAction getAction() {
        return action;
    }

    public String getActionString() {
        return action.getName();
    }

    public String getActionString228() {
        return "action-228";
    }

    public void setAction(VoiceAction action) {
        this.action = action;
    }
}
