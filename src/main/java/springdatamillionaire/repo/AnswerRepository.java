package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Answer;

import java.util.List;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
    public List<Answer> findByQuestion_Id(long id);

}
