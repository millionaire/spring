package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class QuestionType {
    public enum Kind {GAME,PRELIMINARY }

    public QuestionType(Kind kind) {
        this.kind = kind.name();
    }

    public QuestionType() {
    }

    public long getId() {
        return id;
    }

    public Kind getKind() {
        if(Objects.equals(kind, "GAME")) {
            return Kind.GAME;
        }
        else if (Objects.equals(kind, "PRELIMINARY")) {
            return Kind.PRELIMINARY;
        }
        return null;
    }

    public void setKind(Kind kind) {
        this.kind = kind.name();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String kind;
}
