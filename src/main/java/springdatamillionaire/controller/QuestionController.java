package springdatamillionaire.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.DTO.CategoryDTO;
import springdatamillionaire.DTO.QuestionDTO;
import springdatamillionaire.DTO.UserDTO;
import springdatamillionaire.entity.Answer;
import springdatamillionaire.entity.Question;
import springdatamillionaire.service.QuestionService;
import springdatamillionaire.service.UserService;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/questions")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class QuestionController {

    private QuestionService questionService;
    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    List<QuestionDTO> findAll() {
        return questionService.findall();
    }

    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    void addQuestion(@RequestBody @Valid QuestionDTO questionDTO) {
       List<QuestionService.AnswerTuple> answers = new ArrayList<>();
        answers.add(new QuestionService.AnswerTuple(
            questionDTO.getAnswerDTOs().get(0).getText(), questionDTO.getAnswerDTOs().get(0).isRight()));
        answers.add(new QuestionService.AnswerTuple(
            questionDTO.getAnswerDTOs().get(1).getText(), questionDTO.getAnswerDTOs().get(1).isRight()));
        answers.add(new QuestionService.AnswerTuple(
            questionDTO.getAnswerDTOs().get(2).getText(), questionDTO.getAnswerDTOs().get(2).isRight()));
        answers.add(new QuestionService.AnswerTuple(
            questionDTO.getAnswerDTOs().get(3).getText(), questionDTO.getAnswerDTOs().get(3).isRight()));

        questionService.genQuestion(questionDTO.getOrd(), questionDTO.getText(), answers);

    }

    @RequestMapping(value = "/{questionId}", method = RequestMethod.DELETE)
    void delete(@PathVariable long questionId) {
        questionService.delete(questionId);
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    List<CategoryDTO> findAllCategory() {
        return questionService.findallCategoryes();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    void updateQuestion(@RequestBody @Valid QuestionDTO questionDTO) {
        List<String> answers = new ArrayList<>();
        answers.add(0, questionDTO.getAnswerDTOs().get(0).getText());
        answers.add(1, questionDTO.getAnswerDTOs().get(1).getText());
        answers.add(2, questionDTO.getAnswerDTOs().get(2).getText());
        answers.add(3, questionDTO.getAnswerDTOs().get(3).getText());
        questionService.setQuestion(questionDTO.getId(), questionDTO.getOrd(), questionDTO.getText(), answers);

    }




}
