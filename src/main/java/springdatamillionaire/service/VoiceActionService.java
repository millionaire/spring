package springdatamillionaire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springdatamillionaire.entity.VoiceAction;
import springdatamillionaire.entity.VoicePhrase;
import springdatamillionaire.repo.VoiceActionRepository;
import springdatamillionaire.repo.VoicePhraseRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class VoiceActionService {
    private VoiceActionRepository voiceActionRepository;
    private VoicePhraseRepository voicePhraseRepository;

    @Autowired
    public VoiceActionService(
            VoiceActionRepository voiceActionRepository,
            VoicePhraseRepository voicePhraseRepository) {
        this.voiceActionRepository = voiceActionRepository;
        this.voicePhraseRepository = voicePhraseRepository;
    }

    public Map<String, List<String>> getActions() {
        return voiceActionRepository
            .findAll()
            .stream()
            .collect(Collectors.toMap(
                VoiceAction::getName,
                VoiceAction::getPhrasesStrings
            ));
    }

    public Map<String, String> getPhrases() {
        return voicePhraseRepository
            .findAll()
            .stream()
            .collect(Collectors.toMap(
                VoicePhrase::getName,
                VoicePhrase::getActionString
            ));
    }

    public void update(Map<String, List<String>> actions) {
        actions.entrySet().forEach(a -> {
            VoiceAction va = voiceActionRepository.findByName(a.getKey());
            va.clearPhrases();
            a.getValue().forEach(va::addPhrase);
            voiceActionRepository.save(va);
        });
    }

    public void create(Map<String, List<String>> actions) {
        actions.entrySet().forEach(a -> {
            VoiceAction va7 = new VoiceAction(a.getKey());
            a.getValue().forEach(va7::addPhrase);
            voiceActionRepository.save(va7);
        });
    }
}
