package springdatamillionaire.DTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class CategoryDTO {

        public CategoryDTO(long id,String name, int ord) {
            this.id = id;
            this.name = name;
            this.ord = ord;
        }

        public CategoryDTO() {
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

         public int getOrd() {
        return ord;
        }

        public void setOrd(int ord) {
            this.ord = ord;
        }

        private long id;
        @NotNull
        private String name;

        private int ord;
    }
