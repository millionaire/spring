package springdatamillionaire.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.GameHistoryDTO;
import springdatamillionaire.repo.PaymentRepository;
import springdatamillionaire.repo.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentsService {
    private PaymentRepository paymentRepository;
    private UserRepository userRepository;

    @Autowired
    public PaymentsService(
            PaymentRepository paymentRepository,
            UserRepository userRepository) {
        this.paymentRepository = paymentRepository;
        this.userRepository = userRepository;
    }

    public List<GameHistoryDTO> history(long userId) {
        return paymentRepository.findAllByUser(
            userRepository.findOne(userId)
        )
        .stream()
        .map(p -> new GameHistoryDTO(
            p.getId(),
            p.getRoom().getId(),
            p.getMoney()
        ))
        .collect(Collectors.toList());
    }
}