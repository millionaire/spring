package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="voice_action")
public class VoiceAction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="action", orphanRemoval=true)
    private Set<VoicePhrase> phrases;

    /////////////////////////////////

    public VoiceAction() {
    }

    public VoiceAction(String name) {
        this.name = name;
        this.phrases = new HashSet<>();
    }

    public Set<VoicePhrase> getPhrases() {
        return phrases;
    }

    public List<String> getPhrasesStrings() {
        return this.phrases
            .stream()
            .map(VoicePhrase::getName)
            .collect(Collectors.toList());
    }


    public void setPhrases(Set<VoicePhrase> phrases) {
        this.phrases = phrases;
    }

    public void clearPhrases() {
        this.phrases.clear();
    }

    public VoiceAction(String name, Set<VoicePhrase> phrases) {
        this.name = name;
        this.phrases = phrases;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addPhrase(VoicePhrase phrase) {
        phrases.add(phrase);
        phrase.setAction(this);
    }

    public void addPhrase(String phraseString) {
        VoicePhrase phrase = new VoicePhrase(phraseString);
        phrases.add(phrase);
        phrase.setAction(this);
    }
}
