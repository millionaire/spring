package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.VoicePhrase;

import java.util.List;

public interface VoicePhraseRepository extends CrudRepository<VoicePhrase, Long> {
    List<VoicePhrase> findAll();
}
