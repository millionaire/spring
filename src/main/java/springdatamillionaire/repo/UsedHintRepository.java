package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.UsedHints;

import java.util.List;

public interface UsedHintRepository extends CrudRepository<UsedHints,Long> {
    public List<UsedHints> findByUseridAndRoom_Id(long userid,long roomid);
}
