package springdatamillionaire.entity;

import javax.persistence.*;

@Entity
public class AnswerOrder {
    public AnswerOrder(Answer answer, int ord) {
        this.answer = answer;
        this.ord = ord;
    }

    public AnswerOrder() {
    }

    public long getId() {
        return id;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    @OneToOne
    private Answer answer;

    private int ord;
}
