package springdatamillionaire.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class UsedHints {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long userid;
    @ElementCollection
    private List<Integer> hints;
    @ManyToOne
    private Room room;
    UsedHints(Room room, long userid, List<Integer> hints) {
        this.userid = userid;
        this.hints = hints;
        this.room = room;
    }

    public UsedHints() {
    }

    public void add(int i){
        if (hints == null) {
            hints = new ArrayList<>();
        }
        hints.add(i);
    }

    public boolean contains(int i){
        return hints != null && hints.contains(i);
    }

    public List<Integer> getHints() {
        return hints;
    }

    public void setHints(List<Integer> hints) {
        this.hints = hints;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
