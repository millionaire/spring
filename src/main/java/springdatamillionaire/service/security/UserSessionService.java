package springdatamillionaire.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import springdatamillionaire.entity.User;
import springdatamillionaire.repo.UserRepository;
import springdatamillionaire.service.UserRoleService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class UserSessionService {

    @Autowired
    private UserRepository userRepo;

    public SessionUser getUser(String username, String password) {

        // TODO: use boolean exists(ID primaryKey); ???
        List<User> userList = userRepo.findByName(username);

        if (userList.isEmpty()) {
            return null;
        }

        // read 0 index because there is at most 1 user (UNIQUE constraint)
        User user = userList.get(0);

        if (!BCrypt.checkpw(password, user.getSalted_hash())) {
            System.out.println("Someone tried to log in with wrong password");
            return null;
        }

        return getUser(user,password);
    }

    public SessionUser getUser(User user,String password) {
        System.out.println(user.getUserRole().getName());
        return new SessionUser(
                Long.toString(user.getId()),
                user.getName(),
                password,
                Collections.singletonList(new SimpleGrantedAuthority(user.getUserRole().getName())));
    }
}
