package springdatamillionaire.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springdatamillionaire.DTO.StatsDTO;
import springdatamillionaire.DTO.SavedAnswerDTO;
import springdatamillionaire.DTO.SavedAnswersDTO;
import springdatamillionaire.DTO.UserDTO;
import springdatamillionaire.entity.*;
import springdatamillionaire.repo.RoomRepository;
import springdatamillionaire.repo.SavedAnswerRepository;
import springdatamillionaire.repo.UserRepository;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public class SavedAnswerService {
    private SavedAnswerRepository savedAnswerRepository;
    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    private EntityManager em;

    @Autowired
    public SavedAnswerService(
            SavedAnswerRepository savedAnswerRepository,
            UserRepository userRepository,
            UserService userService) {
        this.savedAnswerRepository = savedAnswerRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    public List<SavedAnswer> getSavedAnswers(long roomId, long userId) {
        return savedAnswerRepository.findByUser_IdAndRoom_Id(userId,roomId);
    }
    public SavedAnswerDTO getQuestionSavedAnswer(long roomId, long userId,long questionId) {
        List<SavedAnswer> byRoom_idAndQuestion_id = savedAnswerRepository.findByRoom_IdAndQuestion_Id(roomId, questionId);
        return toSavedAnswerDTO(savedAnswerRepository.findByUser_IdAndRoom_IdAndQuestion_Id(userId, roomId,questionId));
    }

    public List<SavedAnswer> getRoomQuestionSavedAnswer(long roomId,long questionId) {
        return savedAnswerRepository.findByRoom_IdAndQuestion_Id(roomId,questionId);
    }

    public List<SavedAnswer> getRoomSavedAnswer(long roomId){
        return savedAnswerRepository.findByRoom_Id(roomId);
    }
    private SavedAnswerDTO toSavedAnswerDTO(SavedAnswer savedAnswer){
        if (savedAnswer == null) return null;
        return new SavedAnswerDTO(savedAnswer.getQuestion().getId(),savedAnswer.getAnswerId(),savedAnswer.isRight(),savedAnswer.getAnswerTime());
    }

    private SavedAnswersDTO toSavedAnswersDTO(long userId, List<SavedAnswer> savedAnswers) {
        ArrayList<SavedAnswerDTO> savedAnswerDTOS = new ArrayList<SavedAnswerDTO>();
        for(SavedAnswer savedAnswer : savedAnswers) {
            savedAnswerDTOS.add(toSavedAnswerDTO(savedAnswer));
        }
        return new SavedAnswersDTO(userId,savedAnswerDTOS);
    }

    public void save(long answerId,boolean isRight, Room room, long userid, Question question) {
        SavedAnswer s = new SavedAnswer(answerId,isRight,room,userRepository.findOne(userid),question, new DateTime().getMillis());
        savedAnswerRepository.save(s);
    }

    /**
     * Returns list of users that answered question <i>questionId</i> in room <i>roomId</i> correctly (sorted by fastest answer time)
     * @param roomId
     * @param questionId
     * @return
     */
    public List<User> getCorrect(long roomId, long questionId) {
        List<User> users = new ArrayList<>();
        List<SavedAnswer> savedAnswerList = savedAnswerRepository.findByRoom_IdAndQuestion_Id(roomId, questionId);
        savedAnswerList.sort(Comparator.comparingLong(SavedAnswer::getAnswerTime));
        for (SavedAnswer savedAnswer : savedAnswerList) {
            if (savedAnswer.isRight()) {
                users.add(savedAnswer.getUser());
            }
        }
        return users;
    }

    public User getFastestCorrect(long roomId, long questionId) {
        List<User> correctUsers = getCorrect(roomId, questionId);
        if (correctUsers.size() > 0){
            return correctUsers.get(0);
        }
        else {
            return null;
        }
    }

    public List<UserDTO> getPrelimPlayers(long roomId, long questionId, long questionStartTime) {
        ArrayList<UserDTO> userDTOS = new ArrayList<>();
        List<SavedAnswer> savedAnswerList = savedAnswerRepository.findByRoom_IdAndQuestion_Id(roomId,questionId);
        List<User> correctUsers = getCorrect(roomId, questionId);
        User winner = correctUsers.size() > 0 ? correctUsers.get(0) : null;
        for (SavedAnswer savedAnswer: savedAnswerList) {
            UserDTO userDTO = userService.toDTO(savedAnswer.getUser());
            userDTO.setWinner(winner != null && userDTO.getId() == winner.getId());
            userDTO.setCorrect(correctUsers.contains(savedAnswer.getUser()));
            long millis = savedAnswer.getAnswerTime() - questionStartTime;
            userDTO.setAnswerTime(millis / 1000.0);
            userDTOS.add(userDTO);
        }
        return userDTOS;

    }
    @Transactional
    public void clearAnswers(long roomId, long questionId) {
        savedAnswerRepository.removeByRoom_IdAndQuestion_Id(roomId, questionId);
    }

    public boolean allAnswersInRoomUpToCategoryCorrect(long roomId, long userId, int category) {
        List<SavedAnswer> savedAnswers = savedAnswerRepository.findByUser_IdAndRoom_Id(userId,roomId);
        Set<Integer> categories = new HashSet<>();
        for (SavedAnswer savedAnswer: savedAnswers){
            int ord = savedAnswer.getQuestion().getCategory().getOrd();
            if(ord >= 0 && savedAnswer.isRight()) {
                categories.add(ord);
            }
        }
        return categories.size() == category + 1;
    }


//    /**
//     * shitty
//     */
//    public List<StatsDTO> stats(long roomId) {
//        Room room = roomRepository.findOne(roomId);
//        List<StatsDTO> stats =  em.createQuery(
//        "SELECT NEW springdatamillionaire.DTO.StatsDTO(" +
//                "u.name, COUNT(s.user.id), MAX(s.answerTime))" +
//            " FROM SavedAnswer s INNER JOIN s.user u" +
//            " WHERE s.isRight = true AND s.room.id = ?1" +
//            " GROUP BY s.user.id " +
//            " ORDER BY COUNT(s.user.id) DESC, MAX(s.answerTime) ASC",
//            StatsDTO.class
//        )
//        .setParameter(1, roomId)
//        .getResultList();
//
//        // extremely shitty
//        stats.forEach(userStats -> {
//            userStats.setCorrectAnswers(
//                roomService.correctAnswersOneUser(
//                    room,
//                    userRepository.findOneByName(userStats.getUsername())
//                )
//            );
//        });
//        return stats;
//    }
}
