package springdatamillionaire.service;

import me.atrox.haikunator.Haikunator;
import me.atrox.haikunator.HaikunatorBuilder;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.*;
import springdatamillionaire.entity.*;
import springdatamillionaire.repo.*;
import springdatamillionaire.service.security.SessionUser;
import springdatamillionaire.service.security.TokenHandler;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RoomService {


    private static final int MAX_PRELIM_ROUNDS_PLAYED = 20;
    private static final int ROOM_DELETE_DAYS = 7;
    private static final int MAX_NO_FINAL_ANSWER_CATEGORY = 5;

    public void pickAnotherPlayer(long roomId, long userId) {
        Room room = roomRepository.findOne(roomId);
        if(room.getHost().getId() == userId) {
            User newPlayer = selectNewPlayer(roomId);
            room.setActivePlayer(newPlayer);
            roomRepository.save(room);
            if (newPlayer != null) {
                roomOldActivePlayers.add(roomId, newPlayer);
                sendWS(roomId, "ROOMSTATECHANGED", toDTOWithUsers(room));
            } else {
                sendWS(roomId, "NO_MORE_PLAYERS", null);
            }
        }
    }

    public void skipLetsplay(long roomId, long userId) {
        if(roomRepository.findOne(roomId).getHost().getId() == userId) {
            sendWS(roomId, "SKIP_LETSPLAY", null);
        }
    }

    public void startHintCall(long roomId, long userId) {
        if(roomRepository.findOne(roomId).getHost().getId() == userId) {
            sendWS(roomId, "HINTCALLUSED", null);
        }
    }

    private class RoomLinks{
        private class RoomLink{
            private String link;
            private long expirationTime;
            private long roomId;

            RoomLink(String link, long roomId, long expirationTime) {
                this.link = link;
                this.expirationTime = expirationTime;
                this.roomId = roomId;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            long getExpirationTime() {
                return expirationTime;
            }

            public void setExpirationTime(long expirationTime) {
                this.expirationTime = expirationTime;
            }

            long getRoomId() {
                return roomId;
            }

            public void setRoomId(long roomId) {
                this.roomId = roomId;
            }
        }
        private Map<String,RoomLink> roomLinks;

        RoomLinks() {
            roomLinks = new HashMap<>();
        }
        void addLink(String link, long roomId){
            roomLinks.put(link,new RoomLink(link,roomId,DateTime.now().plusHours(2).getMillis()));
        }
        long getRoomIdByLink(String link){
            if (roomLinks.containsKey(link)){
                if (roomLinks.get(link).getExpirationTime() > DateTime.now().getMillis()) {
                    return roomLinks.get(link).getRoomId();
                }
            }
            return -1;
        }
    }
    private class RoomCategories{
        private static final int DEFAULT_CATEGORY = -1;
        private Map<Long,Integer> categoryMap;

        RoomCategories() {
            this.categoryMap = new HashMap<>();
        }

        public void setCategory(long roomId,int category){
            categoryMap.put(roomId,category);
        }

        int getCategory(long roomId){
            return categoryMap.getOrDefault(roomId, DEFAULT_CATEGORY);
        }

        void categoryIncrement(long roomId){
            categoryMap.put(roomId,categoryMap.getOrDefault(roomId, DEFAULT_CATEGORY) + 1);
        }
    }
    private class RoomOldActivePlayers{
        private Map<Long,Set<User>> playerMap;

        RoomOldActivePlayers() {
            this.playerMap = new HashMap<>();
        }

        public void add(long roomId, User user){
            if (!playerMap.containsKey(roomId)) {
                playerMap.put(roomId,new HashSet<>());
            }
            playerMap.get(roomId).add(user);
        }

        public Set<User> get(long roomId){
            return playerMap.get(roomId);
        }
    }

    private static final long QUESTION_TIMER_SECONDS = 26;
    private QuestionService questionService;
    private RoomRepository roomRepository;
    private AnswerService answerService;
    private SimpMessagingTemplate template;
    private SavedAnswerService savedAnswerService;
    private UserRepository userRepository;
    private AnswerOrderRepository answerOrderRepository;
    private RoomLinks roomLinks;
    private UsedHintRepository usedHintRepository;
    private UserService userService;
    private List<Long> invitableRoomsIds;
    private Map<Long, Question> roomsQuestion;//TODO encapsulate
    private Map<Long, List<Long>> roomsWrong5050Ids;//TODO encapsulate
    private RoomCategories roomCategories;
    private RoomOldActivePlayers roomOldActivePlayers;
    private PaymentRepository paymentRopository;

    @Autowired
    public RoomService(QuestionService questionService,
                       RoomRepository roomRepository,
                       AnswerService answerService,
                       SavedAnswerService savedAnswerService,
                       SimpMessagingTemplate template,
                       UserRepository userRepository,
                       AnswerOrderRepository answerOrderRepository,
                       UsedHintRepository usedHintRepository,
                       UserService userService,
                       PaymentRepository paymentRopository) {
        this.questionService = questionService;
        this.roomRepository = roomRepository;
        this.answerService = answerService;
        this.template = template;
        this.savedAnswerService = savedAnswerService;
        this.userRepository = userRepository;
        this.answerOrderRepository = answerOrderRepository;
        this.usedHintRepository = usedHintRepository;
        this.userService = userService;
        this.roomOldActivePlayers = new RoomOldActivePlayers();
        this.roomLinks = new RoomLinks();
        this.roomsQuestion = new HashMap<>();
        this.roomsWrong5050Ids = new HashMap<>();
        this.invitableRoomsIds = new ArrayList<>();
        this.roomCategories = new RoomCategories();
        this.roomsQuestion = new HashMap<>();
        this.paymentRopository = paymentRopository;
    }

    public long getRoomIdByLink(String roomlink) {
        return roomLinks.getRoomIdByLink(roomlink);
    }

    public String generateLink(long roomId,String token) {
        User user = userRepository.findOne(Long.parseLong(TokenHandler.parseSessionUser(token).getUserId()));
        if(roomRepository.findOne(roomId).getHost() == user) {
            Haikunator haikunator = new HaikunatorBuilder().setTokenLength(0).build();
            String link = haikunator.haikunate();
            roomLinks.addLink(link,roomId);
            return link;
        }
        else {
            return "";
        }
    }

    public boolean isRoomInvitable(long roomId) {
        Room room = roomRepository.findOne(roomId);
        return room.getInviteToken() != null;
    }

    public boolean isInviteTokenValid(long roomId, String token) {
        Room room = roomRepository.findOne(roomId);
        return room.getInviteToken().equals(token);
    }

    public void saveInvite(long roomId, String inviteToken, String inviteUrl) {
        Room room = roomRepository.findOne(roomId);
        room.setInviteToken(inviteToken);
        room.setInviteUrl(inviteUrl);
        roomRepository.save(room);
    }

    public boolean checkInviteRights(long roomId, long inviterId) {
        User inviter = userRepository.findOne(inviterId);
        User roomHost = roomRepository.findOne(roomId).getHost();
        return inviter == roomHost;
    }

    public String randomInviteToken() {
        String inviteToken = Base64.getUrlEncoder().encodeToString(
            UUID.randomUUID().toString().getBytes()
        );
        return  inviteToken;
    }

    public void saveToRoomsQuestion(long roomId, Question question) {
        roomsQuestion.put(roomId, question);
    }

    public void continueGame(long roomId, long userId) {
        if(roomRepository.findOne(roomId).getHost().getId() == userId) {
            advanceRoom(roomId);
            sendWS(roomId, "CONTINUE_GAME", null);
        }
    }

    public void sendCorrectAnswer(long roomId, long userId) {
        Room room = roomRepository.findOne(roomId);
        boolean isRight = lastActiveUserAnswerRight(roomId);
        long correctAnswer = getCorrectAnswer(roomsQuestion.get(roomId).getId());
        if(roomRepository.findOne(roomId).getHost().getId() == userId) {
            if(isRight) {
                if (!isFinalQuestion(roomsQuestion.get(roomId).getId())) {
                    sendWS(roomId, "CORRECT_ANSWER", new UserAnswerDTO(correctAnswer));
                } else {
                    sendWS(roomId,"GAMEOVER_WON",
                            new UserAnswerDTO( correctAnswer));
                }
            }
            else{
                User newPlayer = selectNewPlayer(roomId);
                if (newPlayer != null) {
                    room.setActivePlayer(newPlayer);
                    roomOldActivePlayers.add(roomId,newPlayer);
                    roomRepository.save(room);
                    sendWS(roomId,"NEW_PLAYER",
                            new UserAnswerDTO(correctAnswer,
                                    toDTOWithUsers(roomRepository.findOne(roomId))));
                }
                else{
                    sendWS(roomId,"NO_MORE_PLAYERS",
                            new UserAnswerDTO(correctAnswer,
                                    toDTOWithUsers(roomRepository.findOne(roomId))));
                }
            }

        }
    }

    private boolean lastActiveUserAnswerRight(long roomId) {
        Room room = roomRepository.findOne(roomId);
        List<SavedAnswer> savedAnswers = savedAnswerService.getSavedAnswers(roomId,room.getActivePlayer().getId());
        for (SavedAnswer savedAnswer : savedAnswers) {
            if (savedAnswer.getQuestion().getId() == roomsQuestion.get(roomId).getId()) {
                return savedAnswer.isRight();
            }
        }
        return false;
    }

    public void continuePrelim(long roomId, long userId) {
        Room room = roomRepository.findOne(roomId);
        if (userId == room.getHost().getId() && roomCategories.getCategory(roomId) == -1){
            room.setQuestionExpireTime(DateTime.now().plusSeconds(((int) QUESTION_TIMER_SECONDS)).getMillis());
            roomRepository.save(room);
            if (room.getActivePlayer() != null){
                advanceRoom(roomId);
            }
            else{
                long oldQuestionId = roomsQuestion.get(roomId).getId();
                saveToRoomsQuestion(roomId,questionService.randomPreliminaryQuestion(roomId));
                savedAnswerService.clearAnswers(roomId,oldQuestionId);
                roomTimeoutSchedule(roomId);
            }
            sendWS(roomId,"CONTINUEPRELIMQUESTION",null);
        }
    }

    public RoomDTO create(String token) {
        User user = userRepository.findOne(Long.parseLong(TokenHandler.parseSessionUser(token).getUserId()));
        HashSet<User> userHashSet = new HashSet<>();

        // if user join new room, he quit previous one
        Room userRoom = user.getRoom();
        if (userRoom != null) {
                userRoom.removeUser(user);
        }
        Room room = new Room(
            user,
            null,
            false,
            DateTime.now().plusSeconds(((int) QUESTION_TIMER_SECONDS)).getMillis(),
            ""
        );
        user.setRoom(room);

        RoomDTO roomDTO = toDTOWithUsers(roomRepository.save(room));
        roomsQuestion.put(room.getId(), questionService.randomPreliminaryQuestion(room.getId()));
        return roomDTO;
}

    public void delete(long roomId, String token){
        User user = userRepository.findOne(Long.parseLong(TokenHandler.parseSessionUser(token).getUserId()));
        if(roomRepository.findOne(roomId).getHost() == user) {
            roomRepository.delete(roomId);
        }
    }


    public QuestionDTO getQuestionDTO(long roomId) {
        Question q = getQuestion(roomId);
        return new QuestionDTO(
            q.getId(),
            q.getText(),
            mark5050WrongAnswers(roomId,answerService.findAnswersByQuestionId(q.getId())),
            q.getQuestionType().getKind() == QuestionType.Kind.PRELIMINARY,
            q.getCategory().getOrd()
        );
    }

    private List<AnswerDTO> mark5050WrongAnswers(long roomId, List<AnswerDTO> answersByQuestionId) {
        answersByQuestionId.forEach(answerDTO -> answerDTO.setWrong5050(
                roomsWrong5050Ids.get(roomId) != null && roomsWrong5050Ids.get(roomId).contains(answerDTO.getId())
        ));

        return answersByQuestionId;
    }

    private Question getQuestion(long roomId) {
        int category = roomCategories.getCategory(roomId);
        if(category < 15) {
            Question currentQuestion = roomsQuestion.get(roomId);
            return currentQuestion;
        }
        else {
            return new Question();
        }
    }

    public void checkAndSaveAnswer(UserAnswerDTO userAnswerDTO, long roomId, String token) {
        long questionId = getQuestionDTO(roomId).getId();
        SessionUser user = TokenHandler.parseSessionUser(token);
        long userId = Long.parseLong(TokenHandler.parseSessionUser(token).getUserId());
        Room room = roomRepository.findOne(roomId);
        boolean isPreliminary = getQuestionDTO(roomId).isPreliminary();
        long hostId = room.getHost().getId();
        long activePlayerId = room.getActivePlayer() == null ? -1: room.getActivePlayer().getId() ;
        long answererId = userId == hostId ? activePlayerId : userId;

        User u = userRepository.findOne(userId);
        boolean isRight = answerIsRight(userAnswerDTO, questionId, isPreliminary);
        if ((isPreliminary && userId != hostId && DateTime.now().getMillis() < room.getQuestionExpireTime())||
                (!isPreliminary && userId != activePlayerId)) {


            if (savedAnswerService.getQuestionSavedAnswer(roomId, answererId, questionId) == null) {
                if (!isPreliminary && userId == hostId ) {
                    sendWS(roomId, "ACTIVE_PLAYER_ANSWER_CLICKED",
                            userAnswerDTO);
                }
                saveAnswer(questionId,userAnswerDTO.getAnswerId(), isRight, roomId, answererId);

                if (!isRight && paymentRopository.findOneByRoomAndUser(room, u) == null) { // user failed first time in room
                    giveMoney(room, u);
                }
            }
        }
    }

    private long getCorrectAnswer(long questionId) {
        return answerService.findCorrectAnswerIdByQuestionId(questionId);
    }

    private boolean isFinalQuestion(long questionId) {
        return questionService.findById(questionId).getCategory().getOrd() == 14;
    }

    private boolean answerIsRight(UserAnswerDTO userAnswerDTO, long questionId, boolean isPreliminary) {
        boolean isRight;
        if (isPreliminary) {
            isRight = isRightPreliminary(userAnswerDTO, questionId);
            userAnswerDTO.setAnswerId(0);
        } else {
            isRight = getCorrectAnswer(questionId) == userAnswerDTO.getAnswerId();
        }
        return isRight;
    }
    private User selectNewPlayer(long roomId) {
        long questionId = getQuestion(roomId).getId();
        List<User> correct = savedAnswerService.getCorrect(roomId, questionId); //sorted by answer time
        Set<User> users = roomOldActivePlayers.get(roomId);
        if (users != null) { //happens after the server restarts mid game
            correct.removeAll(users);
        }
        for (User user : correct){
            if (savedAnswerService.allAnswersInRoomUpToCategoryCorrect(roomId,user.getId(),roomsQuestion.get(roomId).getCategory().getOrd())) {
                return user;
            }
        }
        return null;
    }

    private void endPrelimRound(long roomId) {
        Room room = roomRepository.findOne(roomId);
        if (roomCategories.getCategory(roomId) == -1) {
            long questionId = getQuestion(roomId).getId();
            long questionStartTime = room.getQuestionExpireTime() - QUESTION_TIMER_SECONDS * 1000;
            User winner = savedAnswerService.getFastestCorrect(roomId,questionId);
            room.setPrelimRoundsPlayed(room.getPrelimRoundsPlayed() + 1);

            if (winner != null) {
                room.setActivePlayer(winner);
                roomOldActivePlayers.add(roomId,winner);
            }
            roomRepository.save(room);

            sendWS(roomId,"ENDPRELIMQUESTION",
                    new UserAnswerDTO(getCorrectAnswerPairs(questionId), getPrelimPlayers(roomId,questionId,questionStartTime)));
        }
        else{
            //TODO stop the timer
        }
    }

    private void advanceRoom(long roomId) {
        Room updated = roomRepository.findOne(roomId);
        updated.setQuestionExpireTime(DateTime.now().getMillis() + QUESTION_TIMER_SECONDS * 1000);
        roomRepository.save(updated);

        roomCategories.categoryIncrement(roomId);
        saveToRoomsQuestion(roomId,questionService.randomQuestion(roomCategories.getCategory(roomId)));
    }

    private List<UserDTO> getPrelimPlayers(long roomId, long questionId, long questionStartTime) {
        return savedAnswerService.getPrelimPlayers(roomId,questionId,questionStartTime);
    }

    private List<PreliminaryAnswerPair> getCorrectAnswerPairs(long questionId){
        List<AnswerOrder> answerOrders = answerOrderRepository.findByAnswer_Question_Id(questionId);
        ArrayList<PreliminaryAnswerPair> correctPairs = new ArrayList<>();
        for (AnswerOrder answerOrder: answerOrders) {
            correctPairs.add(new PreliminaryAnswerPair(answerOrder.getAnswer().getId(),answerOrder.getOrd()));
        }
        return correctPairs;
    }

    private boolean isRightPreliminary(UserAnswerDTO userAnswerDTO, long questionId) {
        boolean isRight;
        List<AnswerOrder> answerOrders = answerOrderRepository.findByAnswer_Question_Id(questionId);
        isRight = userAnswerDTO.getAnswerPairs() != null && answerOrders.size() == userAnswerDTO.getAnswerPairs().size();
        for (AnswerOrder answerOrder: answerOrders){
            boolean found = false;
            if (userAnswerDTO.getAnswerPairs() != null) {
                for (PreliminaryAnswerPair answerPair : userAnswerDTO.getAnswerPairs()) {
                    if (answerPair.getAnswerId() == answerOrder.getAnswer().getId()) {
                        found = answerPair.getOrd() == answerOrder.getOrd();
                        break;
                    }
                }
            }
            isRight &= found;
        }
        return isRight;
    }

    private void saveAnswer(long questionId,long answerId, boolean isRight, long roomId,long userId) {
        savedAnswerService.save(answerId,isRight,roomRepository.findOne(roomId),userId,
                questionService.findById(questionId));
    }


    public List<RoomDTO> findall() {
        Iterable<Room> rooms = roomRepository.findByFinished(false);
        ArrayList<RoomDTO> userDTOS = new ArrayList<>();
        for (Room room : rooms) {
            userDTOS.add(toDTOWithUsers(room));
        }
        return userDTOS;
    }

    public RoomDTO joinRoom(long roomId, long userId) {
        addUser(roomId, userId, "");
        RoomDTO roomDTO = toDTOWithUsers(roomRepository.findOne(roomId));
        sendWS(roomId,"ROOMSTATECHANGED",roomDTO);
        return roomDTO;
    }
    public RoomDTO findOne(long roomId) {
        return toDTOWithUsers(roomRepository.findOne(roomId));
    }

    public void getHint(long roomId, String token,Hint hint){
        User user = userRepository.findOne(Long.parseLong(TokenHandler.parseSessionUser(token).getUserId()));
        Room room = roomRepository.findOne(roomId);
        QuestionDTO questionDTO = getQuestionDTO(roomId);
        if (room.getHost().getId() == user.getId()) {
            if (!getHintUsed(room.getId(),room.getActivePlayer().getId(), hint) && !questionDTO.isPreliminary()) {
                setHintUsed(room.getId(),room.getActivePlayer().getId(),hint);
                Object reply = new Object();
                switch (hint) {
                    case HINT5050:
                        List<Long> wrong5050Ids = toHint5050DTO(questionDTO);
                        roomsWrong5050Ids.put(roomId,wrong5050Ids);
                        reply = wrong5050Ids;
                        break;
                    case HINTPOLL:
                        sendWS(roomId,"HINTPOLLSTARTED",null);
                        new Timer().schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        sendWS(roomId,hint.name() + "USED", toHintPollDTO(roomId,questionDTO));
                                    }
                                },32000
                        );
                        return;
                    case HINTCALL:
                        sendWS(roomId,"HINTCALLSTARTED",null);
                        return;
                }
                sendWS(roomId,hint.name() + "USED",reply);
            }
        }

    }

    private List<Long> toHint5050DTO(QuestionDTO question) {
        List<AnswerDTO> answerDTOs = question.getAnswerDTOs();
        List<Long> wrong5050Ids = new ArrayList<>();
        if (answerDTOs.size() > 0) {
            shuffleList(answerDTOs);
            int i = 0;
            while(i < 2) {
                if (answerService.checkAnswerById(answerDTOs.get(i).getId())) {
                    answerDTOs.remove(i);
                }
                wrong5050Ids.add(answerDTOs.get(i).getId());
                i++;
            }
        }
        return wrong5050Ids;
    }


    private HintPollDTO toHintPollDTO(long roomId, QuestionDTO questionDTO) {
//        int correct = -1;
//        Random random = new Random();
//        List<AnswerDTO> answerDTOs = questionDTO.getAnswerDTOs();
//        double totalpercent = 100.0;
//        List<AnswerPercent> answerPercents = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            double randValue = random.nextDouble() * 25;
//            totalpercent -= randValue;
//            long answerId = answerDTOs.get(i).getId();
//            answerPercents.add(new AnswerPercent(answerId,randValue));
//            if (answerService.checkAnswerById(answerId)) {
//                correct = i;
//            }
//        }
//        answerPercents.get(correct).setPercent(answerPercents.get(correct).getPercent() + totalpercent);

        List<SavedAnswer> savedAnswers = savedAnswerService.getRoomQuestionSavedAnswer(roomId,questionDTO.getId());
        List<AnswerPercent> answerPercents = new ArrayList<>();
        List<AnswerDTO> answerDTOs = questionDTO.getAnswerDTOs();
        for (AnswerDTO answerDTO: answerDTOs){
            double answercount = savedAnswers.stream().filter(
                    ans -> ans.getAnswerId() == answerDTO.getId())
                    .count();
            double answerpercent = (answercount != 0) ? (answercount * 100.0/ (long)savedAnswers.size()) : 0;
            answerPercents.add(new AnswerPercent(answerDTO.getId(),
                    answerpercent));
        }
        return new HintPollDTO(answerPercents);
    }
    private void sendWS(long roomId,String message, Object payload) {
        template.convertAndSend("/topic/room" + roomId,new WSWrapper(message,payload));
    }

    private static void shuffleList(List a) {
        int n = a.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(a, i, change);
        }
    }

    private static void swap(List a, int i, int change) {
        Object helper = a.get(i);
        a.set(i, a.get(change));
        a.set(change, helper);
    }

    private RoomDTO toDTO(Room room) {
        long activePlayerId = room.getActivePlayer() == null ? -1 : room.getActivePlayer().getId();
        return new RoomDTO(
            room.getId(),
            roomCategories.getCategory(room.getId()),
            room.getActivePlayer() != null ? room.getActivePlayer().getId() : -1,
            room.getHost() != null ? room.getHost().getId() : -1,
            room.isActive(),
            room.getInviteUrl(),
                getHintUsed(room.getId(),activePlayerId,Hint.HINT5050),
                getHintUsed(room.getId(),activePlayerId,Hint.HINTPOLL),
                getHintUsed(room.getId(),activePlayerId,Hint.HINTCALL));
//                getHintUsed(room,Hint.HINT5050),
//                getHintUsed(room,Hint.HINTPOLL),
//                getHintUsed(room,Hint.HINTCALL));
    }

    private RoomDTO toDTOWithUsers(Room room) {
        RoomDTO roomDTO = toDTO(room);
        roomDTO.setUsers(userService.findByRoomId(room.getId()));
        return roomDTO;
    }

//
//    public SavedAnswersDTO getSavedAnswers(long roomId, long userId) {
//        return savedAnswerService.getSavedAnswers(roomId,userId);
//    }

    private void addUser(long roomId, Long userId, String roomPassword) {
        User user = userRepository.findOne(userId);
        Room room = roomRepository.findOne(roomId);

        // if user join new room, he quit previous one
        Room oldRoom = user.getRoom();
        if (oldRoom != null) {
            oldRoom.removeUser(user);
            roomRepository.save(oldRoom);
            sendWS(oldRoom.getId(),"ROOMSTATECHANGED",toDTOWithUsers(roomRepository.findOne(oldRoom.getId())));
        }

        // if room has password and password wrong
        if(!room.getPass_hash().equals("") && !BCrypt.checkpw(roomPassword, room.getPass_hash())) {
            System.out.println("wrong room-password");
            return;
        }

        room.addUser(user);
        roomRepository.save(room);
    }

    public void startGame(long roomId, long userId) {
        Room room = roomRepository.findOne(roomId);
        if (!room.isActive()) {
            User user = userRepository.findOne(userId);

            if (room.getHost() == null || room.getHost().equals(user)) {
                sendWS(roomId, "GAMESTART", null);
            }

            room.setActive(true);
            room.setQuestionExpireTime(DateTime.now().plusSeconds(((int) QUESTION_TIMER_SECONDS)).getMillis());
            roomTimeoutSchedule(roomId);
            roomRepository.save(room);
        }
    }

    private void roomTimeoutSchedule(long roomId) {
        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        if (roomRepository.findOne(roomId).getPrelimRoundsPlayed() < MAX_PRELIM_ROUNDS_PLAYED){
                            endPrelimRound(roomId);
                        }
                    }
                },
                QUESTION_TIMER_SECONDS * 1000
        );
    }


    @Autowired
    private EntityManager em;



    public String stopGame(long roomId, long userId) {
        Room room = roomRepository.findOne(roomId);
        User user = userRepository.findOne(userId);

        if(!room.getHost().equals(user)) {
            return "only host can stop game";
        }

        room.setActive(false);
        room.setFinished(true);
        room.setGameOverTimestamp(DateTime.now().getMillis());

        userRepository.findByRoom_Id(roomId).forEach(u -> {
            u.setRoom(null);
            // user did not get money in this room
            if (paymentRopository.findOneByRoomAndUser(room, u) == null && u != room.getHost()) {
                giveMoney(room, u);
            }
        });

        room.clearUsers();

        roomRepository.save(room);
        sendWS(roomId, "STOP_GAME", null);

        return "stop game in room" + roomId + "successful";
    }

    public int correctAnswersOneUser(Room room, User user) {
        List answers = em.createQuery(
                "SELECT s.isRight" +
                        " FROM SavedAnswer s" +
                        " WHERE s.room = ?1 AND s.user = ?2 AND s.question.category.id <> 16" +
                        " ORDER BY s.answerTime"
        )
                .setParameter(1, room)
                .setParameter(2, user)
                .getResultList();
        return answers.indexOf(false) == -1 ? answers.size() : answers.indexOf(false);
    }


    /**
     * ugly, shitty AF
     */
    public List<StatsDTO> stats(long roomId) {
        Room room = roomRepository.findOne(roomId);

        List<StatsDTO> stats = new ArrayList<>();

        room.getUsers().forEach(user -> {
            if (user != room.getHost()) {

                List answers = em.createQuery(
                        "SELECT s.isRight" +
                                " FROM SavedAnswer s" +
                                " WHERE s.room = ?1 AND s.user = ?2 AND s.question.category.id <> 16" +
                                " ORDER BY s.answerTime"
                )
                        .setParameter(1, room)
                        .setParameter(2, user)
                        .getResultList();

                int correctAnswers;
                int money = 0;
                boolean failed;
                if (answers.indexOf(false) == -1) {
                    correctAnswers = answers.size();
                    switch (correctAnswers) {
                        case  1: money =     100; break;
                        case  2: money =     200; break;
                        case  3: money =     300; break;
                        case  4: money =     500; break;
                        case  5: money =    1000; break;
                        case  6: money =    2000; break;
                        case  7: money =    4000; break;
                        case  8: money =    8000; break;
                        case  9: money =   16000; break;
                        case 10: money =   32000; break;
                        case 11: money =   64000; break;
                        case 12: money =  125000; break;
                        case 13: money =  250000; break;
                        case 14: money =  500000; break;
                        case 15: money = 1000000; break;
                        default: money =       0; break;
                    }
                    failed = false;
                }
                else {
                    failed = true;
                    correctAnswers = answers.indexOf(false);
                    if      (correctAnswers  <  5) { money =       0; }
                    else if (correctAnswers  < 10) { money =    1000; }
                    else if (correctAnswers  < 15) { money =   32000; }
                    else if (correctAnswers == 15) { money = 1000000; }
                }

                stats.add(new StatsDTO(user.getName(), correctAnswers, money, failed));
            }
        });

        return stats;


//        List<StatsDTO> stats = em.createQuery(
//                "SELECT NEW springdatamillionaire.DTO.StatsDTO(" +
//                        "u.name, correctAnswersOneUser(room, u), MAX(s.answerTime))" +
//                        " FROM SavedAnswer s INNER JOIN s.user u" +
//                        " WHERE s.isRight = true AND s.room = ?1" +
//                        " GROUP BY s.user.id " +
//                        " ORDER BY COUNT(s.user.id) DESC, MAX(s.answerTime) ASC",
//                StatsDTO.class
//        )
//                .setParameter(1, room)
//                .getResultList();
//
//         extremely shitty
//        stats.forEach(userStats -> {
//            userStats.setCorrectAnswers(
//                    correctAnswersOneUser(
//                            room,
//                            userRepository.findOneByName(userStats.getUsername())
//                    )
//            );
//        });
//        return stats;
    }


    public void giveMoney(Room room, User user) {

        int correctAnswers = correctAnswersOneUser(room, user);

        int money = 0;

        if      (correctAnswers  <  5) { money =       0; }
        else if (correctAnswers  < 10) { money =    1000; }
        else if (correctAnswers  < 15) { money =   32000; }
        else if (correctAnswers == 15) { money = 1000000; }

        Payment payment = new Payment(room, user, money);
        paymentRopository.save(payment);


        user.addMoney(money);
        user.addPayment(payment);
        userRepository.save(user);

        room.addPayment(payment);
        roomRepository.save(room);

        System.out.println("from GIVE MONEY");
    }

//      @Scheduled(cron = "0 0 * * *") // every midnight
////  @Scheduled(fixedRate = 100000) // 100s
////    @Scheduled(fixedRate = 10000)  // 10s
//    public void deleteStaleRooms() {
//
//        List<Room> allRooms = roomRepository.findAll(); // TODO: use sql query, instead of filter in java application
//        List<Room> stale = allRooms.stream()
//                .filter(u -> u.isActive() == false && u.getGameOverTimestamp() != 0 && u.getGameOverTimestamp() + 1000L*60L < DateTime.now().getMillis())
//                .collect(Collectors.toList());
//        roomRepository.delete(stale);
//    }
//

    private void setHintUsed(long roomid, long userId, Hint hint) {
        List<UsedHints> usedHints = usedHintRepository.findByUseridAndRoom_Id(userId,roomid);
        Room room = roomRepository.findOne(roomid);
        if (usedHints.size() > 0){
            usedHints.get(0).add(hint.ordinal());
            usedHintRepository.save(usedHints.get(0));
        }
        else {
            UsedHints usedHint = new UsedHints();
            usedHint.add(hint.ordinal());
            usedHint.setUserid(userId);
            usedHint.setRoom(room);
            room.getUsedHints().add(usedHint);
            roomRepository.save(room);
        }
    }

    private boolean getHintUsed(long roomid,long userId, Hint hint) {
        List<UsedHints> usedHints = usedHintRepository.findByUseridAndRoom_Id(userId,roomid);
        return usedHints.size() > 0 && usedHints.get(0).contains(hint.ordinal());
    }
//    private boolean getHintUsed(Room room, Hint hint) {
//        UsedHints usedHint = usedHintRepository.findByOrdAndRoom_Id(roomCategories.getCategory(room.getId()), room.getId());
//        return usedHint != null && usedHint.contains(hint.ordinal());
//    }
    @Scheduled(cron = "0 0 0 * * *")
    private void roomCleanup(){
        List<Room> roomsToDelete = new ArrayList<>();
        roomRepository.findAll().forEach(room -> {
            if (!room.isActive() &&
                    (room.getQuestionExpireTime()) < DateTime.now().minusDays(ROOM_DELETE_DAYS).getMillis())
            {
                roomsToDelete.add(room);
            }
        });
        roomRepository.delete(roomsToDelete);
    }
}
