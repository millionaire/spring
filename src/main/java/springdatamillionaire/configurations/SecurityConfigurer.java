package springdatamillionaire.configurations;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import springdatamillionaire.service.security.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final TokenAuthenticationService authenticationService;

    @Autowired
    public SecurityConfigurer(TokenAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        System.out.println("Hello from SecurityConfigurer.configure");
        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/gs-guide-websocket").permitAll()
                .antMatchers("/gs-guide-websocket/**").permitAll()
                .antMatchers("/api/rooms/invite/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/rooms").permitAll()
                .antMatchers("/api/rooms/{roomId}/question").permitAll()
                .antMatchers("/api/rooms/{roomId}/stats").permitAll()
                .antMatchers("/api/rooms/{roomId}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/users/{userId}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/users").permitAll()
                .antMatchers("/api/voice/update").hasAuthority("admin")
                .antMatchers(HttpMethod.POST, "/api/users").permitAll()
                .antMatchers("/admin/**").hasAuthority("admin")
                .antMatchers("/api/users/{userId}/games_history").permitAll()
                .antMatchers(HttpMethod.PUT,"/users/**").hasAuthority("admin")
                .antMatchers(HttpMethod.DELETE,"/users/**").hasAuthority("admin")
                .anyRequest().not().hasAuthority("banned")
//                .anyRequest().authenticated()
                .and()
                .cors().and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
            .addFilterBefore(
                new JWTAuthenticationFilter(authenticationService),
                UsernamePasswordAuthenticationFilter.class
            );
    }
}
