package springdatamillionaire.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.service.VoiceActionService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/voice")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VoiceActionController {

    private VoiceActionService voiceActionService;

    @Autowired
    public VoiceActionController(VoiceActionService voiceActionService) {
        this.voiceActionService = voiceActionService;
    }

    @GetMapping("actions")
    @ResponseStatus(HttpStatus.OK)
    Map<String, List<String>> getActions() {
        return voiceActionService.getActions();
    }

    @GetMapping("phrases")
    @ResponseStatus(HttpStatus.OK)
    Map<String, String> getPhrases() {
        return voiceActionService.getPhrases();
    }

    @PostMapping("update")
    @ResponseStatus(HttpStatus.OK)
    void getPhrases(@RequestBody Map<String, List<String>> actions) {
        voiceActionService.update(actions);
    }

    @PostMapping("create")
    @ResponseStatus(HttpStatus.OK)
    void create(@RequestBody Map<String, List<String>> actions) {
        voiceActionService.create(actions);
    }
}
