package springdatamillionaire.DTO;

import java.util.List;

public class Hint5050DTO {
    private List<AnswerDTO> answerDTOs;

    public Hint5050DTO(List<AnswerDTO> answerDTOs) {
        this.answerDTOs = answerDTOs;
    }

    public List<AnswerDTO> getAnswerDTOs() {
        return answerDTOs;
    }

    public void setAnswerDTOs(List<AnswerDTO> answerDTOs) {
        this.answerDTOs = answerDTOs;
    }
}
