package springdatamillionaire.service.googleurlshortener;

public class RequestBody {
    public String longUrl;

    public RequestBody(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }
}
