package springdatamillionaire.service;

import me.atrox.haikunator.Haikunator;
import me.atrox.haikunator.HaikunatorBuilder;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.UserDTO;
import springdatamillionaire.entity.User;
import springdatamillionaire.repo.UserRepository;
import springdatamillionaire.repo.UserRoleRepository;
import springdatamillionaire.service.security.SessionUser;
import springdatamillionaire.service.security.UserSessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class UserService {

    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;
    private UserSessionService userSessionService;

    @Autowired
    public UserService(UserRepository userRepository, UserRoleRepository userRoleRepository, UserSessionService userSessionService) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.userSessionService = userSessionService;
    }


    public User create(UserDTO userDTO) {
        User user = new User(
            userDTO.getName(),
            userRoleRepository.findOne(2L), // TODO: maybe we need just use "user", why we need table for user roles???
            BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt())
        );
        userRepository.save(user);
        return user;
    }

    public SessionUser createTempUser() {
        Haikunator haikunator = new HaikunatorBuilder().setTokenLength(0).setDelimiter("-").build();

        String password = haikunator.haikunate();

        User user = new User(
            haikunator.haikunate(),
            userRoleRepository.findByName("temp"),
            BCrypt.hashpw(password, BCrypt.gensalt())
        );
        userRepository.save(user);

        return userSessionService.getUser(user, password);
    }

    public List<UserDTO> findall()
    {
        List<User> users = userRepository.findAll();
        ArrayList<UserDTO> userDTOS = new ArrayList<UserDTO>();
        for (User user : users) {
            userDTOS.add(toDTO(user));
        }
        return userDTOS;
    }

    public UserDTO findById(long id)
    {
        return toDTO(userRepository.findOne(id));
    }

    public UserDTO update(UserDTO userDTO,long userId) {
        User updated = userRepository.findOne(userId);
        updated.setName(userDTO.getName());
        updated.setUserRole(userRoleRepository.findOne(userDTO.getUserRoleId()));

        if (userDTO.getUserRoleId() == 3) {
            updated.setBanTimestamp(DateTime.now().getMillis());
        }

        return toDTO(userRepository.save(updated));
    }

    @Scheduled(cron = "0 0 0 * * *")
    private void deleteStaleBanned() {
        List<User> allUsers = userRepository.findAll(); // TODO: use sql query, instead of filter in java application
        List<User> staleBanned = allUsers.stream()
                .filter(u -> u.getUserRole().getId() == 3 && u.getBanTimestamp() + 1000L*60L < DateTime.now().getMillis())
                .collect(Collectors.toList());

        userRepository.delete(staleBanned);
    }



    // TODO: return boolean
    public void delete(long id) {
        userRepository.delete(id);
    }

    public List<UserDTO> findByRoomId(long roomid){
        ArrayList<UserDTO> userDTOS = new ArrayList<>();
        List<User> users = userRepository.findByRoom_Id(roomid);
        for(User user: users){
            userDTOS.add(toDTO(user));
        }
        return userDTOS;
    }

    // private User fromDTO(UserDTO userDTO) {
    //     UserRole userRole = userRoleRepository.findOne(userDTO.getUserRoleId());
    //
    //     return new User(
    //         userDTO.getName(),
    //         userDTO.getSurname(),
    //         userRole,
    //         userDTO.getPassword()
    //     );
    // }

    public UserDTO toDTO(User user) {
        return new UserDTO(
            user.getId(),
            user.getName(),
            user.getUserRole().getName(),
            user.getUserRole().getId(),
            user.getMoney()
        );
    }

    public void delete_all()
    {
        userRoleRepository.deleteAll();
    }

    public long userRoomId(long userId) {
        User user = userRepository.findOne(userId);

        if (user.getRoom() == null) {
            return 0; // id = 0 means user has no room (by default MySQL doesn't use 0 for autoincrement ids)
        }
        else {
            return user.getRoom().getId();
        }
    }

}
