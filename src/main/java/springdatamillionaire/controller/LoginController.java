package springdatamillionaire.controller;

import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import springdatamillionaire.service.security.UserSessionService;
import springdatamillionaire.service.security.SessionUser;
import springdatamillionaire.service.security.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders="access_token")
public class LoginController {

    private final TokenAuthenticationService authenticationService;
    private final UserSessionService userSessionService;

    @Autowired
    public LoginController(TokenAuthenticationService authenticationService,
                           UserSessionService userSessionService) {
        this.authenticationService = authenticationService;
        this.userSessionService = userSessionService;
    }

    @PostMapping(value = "/login")
    public String login(@RequestBody Map<String, String> jsonBody, HttpServletResponse response) {

        String username = jsonBody.get("username");
        String password = jsonBody.get("password");

        System.out.println(username + ' ' + password);

        // SessionUser user = (SessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        SessionUser sessionUser = userSessionService.getUser(username, password);
        if (sessionUser == null) {
            response.setStatus(403);
            return "User not found";
        } else {
            for (GrantedAuthority grantedAuthority: sessionUser.getAuthorities()) {
                if (grantedAuthority.getAuthority().equals("banned")) {
                    response.setStatus(403);
                    return "Banned";
                }
                else if (grantedAuthority.getAuthority().equals("mailunconfirmed")) {
                    response.setStatus(403);
                    return "Mail not confirmed";
                }
            }
            authenticationService.addAuthentication(response, sessionUser);
            response.setStatus(200);
            return "Login Success";
        }
    }

    @GetMapping("/login_check")
    @ResponseStatus(value = HttpStatus.OK)
    public void login_check() {
    }

    @GetMapping("/secret_url")
    @ResponseStatus(value = HttpStatus.OK)
    public String someEndpoint() {
        return "Secret data from secret API";
    }
}
