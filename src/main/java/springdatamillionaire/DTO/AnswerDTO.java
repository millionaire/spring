package springdatamillionaire.DTO;

public class AnswerDTO {
    private long id;
    private String text;
    private boolean right;
    private boolean wrong5050;

    public AnswerDTO(long id, String text, boolean right) {
        this.id = id;
        this.text = text;
        this.right = right;
    }
    public AnswerDTO() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isWrong5050() {
        return wrong5050;
    }

    public void setWrong5050(boolean wrong5050) {
        this.wrong5050 = wrong5050;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }
}
