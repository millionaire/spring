package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Question;
import springdatamillionaire.entity.Room;
import springdatamillionaire.entity.User;
import java.util.List;

public interface RoomRepository extends CrudRepository<Room, Long> {
    List<Room> findAll();
    List<Room> findByFinished(boolean finished);
    // public Question findByIdAndQuestions_Category_Ord(long id,int ord);
    // public List<Room> findByQuestions_Id(long questionId);
}
