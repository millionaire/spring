package springdatamillionaire.DTO;

public class AnswerPercent {
    private long id;
    private double percent;

    public AnswerPercent(long id, double percent) {
        this.id = id;
        this.percent = percent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }
}