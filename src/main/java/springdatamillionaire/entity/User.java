package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
public class User {

    // variables
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true)
    @NotNull
    private String name;


    @ManyToOne
    private UserRole userRole;

    private String salted_hash;

    @ManyToOne
    private Room room;

    private int money = 0;

    @OneToMany
    protected Set<Payment> payments;

    public void addPayment(Payment payment) {
        this.payments.add(payment);
    }

    public int getMoney() {
        return money;
    }

    public void addMoney(int money) {
        System.out.println("hello from addMoney");
        this.money += money;
    }

    private long banTimestamp;

    public long getBanTimestamp() {
        return banTimestamp;
    }

    public void setBanTimestamp(long banTimestamp) {
        this.banTimestamp = banTimestamp;
    }



    // constructors
    public User( String name, UserRole userRole, String salted_hash) {
        this.name = name;
        this.userRole = userRole;
        this.salted_hash = salted_hash;
    }

    public User() {
    }

    // getters and setters
    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public UserRole getUserRole() {
        return userRole;
    }
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
    public Room getRoom() {
        return room;
    }
    public void setRoom(Room room) {
        if (this.room != room){
            if (this.room != null){
                this.room.removeUser(this);
            }
            if (room != null){
                room.addUser(this);
            }
            this.room = room;
        }
    }
    public String getSalted_hash() {
        return salted_hash;
    }
    public void setSalted_hash(String salted_hash) {
        this.salted_hash = salted_hash;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userRole=" + userRole.getName() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
