package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Room {
    public Room(User host, User activePlayer, boolean active, long questionExpireTime, String pass_hash) {
        this.host = host;
        this.activePlayer = activePlayer;
        this.active = active;
        this.questionExpireTime = questionExpireTime;
        this.pass_hash = pass_hash;
        this.users = new HashSet<>();
        this.usedHints = new HashSet<>();
        this.prelimRoundsPlayed = 0;
        this.finished = false;
    }

    public Room() {
        this.finished = false;
        this.users = new HashSet<>();
        this.usedHints = new HashSet<>();
        this.prelimRoundsPlayed = 0;
    }

    public long getId() {
        return id;
    }

    public User getHost() {
        return host;
    }

    public void setHost(User host) {
        this.host = host;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPass_hash() {
        return pass_hash;
    }

    public void setPass_hash(String pass_hash) {
        this.pass_hash = pass_hash;
    }

    public void addUser(User user){
        if (user != null){
            if (users.contains(user)){
                return;
            }
            users.add(user);
            user.setRoom(this);
        }
    }

    public void removeUser(User user){
        if (user != null){
            if (!users.contains(user)){
                return;
            }
            users.remove(user);
            user.setRoom(null);
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @OneToOne
    private User host;

    @OneToOne
    private User activePlayer;

    private boolean active;
    private boolean finished;
    private long questionExpireTime;
    private int prelimRoundsPlayed;

    @NotNull
    private String pass_hash;

    public String getInviteToken() {
        return inviteToken;
    }

    public void setInviteToken(String inviteToken) {
        this.inviteToken = inviteToken;
    }

    private String inviteToken;
    private String inviteUrl;

    public String getInviteUrl() {
        return inviteUrl;
    }

    public void setInviteUrl(String inviteUrl) {
        this.inviteUrl = inviteUrl;
    }

    @OneToMany
    private Set<User> users;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    private Set<UsedHints> usedHints;

    @OneToMany
    private Set<Payment> payments;

    public void addPayment(Payment payment) {
        this.payments.add(payment);
    }

    public long getGameOverTimestamp() {
        return gameOverTimestamp;
    }

    public void setGameOverTimestamp(long gameOverTimestamp) {
        this.gameOverTimestamp = gameOverTimestamp;
    }

    private long gameOverTimestamp;


    public User getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(User activePlayer) {
        this.activePlayer = activePlayer;
    }

    public long getQuestionExpireTime() {
        return questionExpireTime;
    }

    public void setQuestionExpireTime(long questionExpireTime) {
        this.questionExpireTime = questionExpireTime;
    }


    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<UsedHints> getUsedHints() {
        return usedHints;
    }

    public void setUsedHints(Set<UsedHints> usedHints) {
        this.usedHints = usedHints;
    }

    public int getPrelimRoundsPlayed() {
        return prelimRoundsPlayed;
    }

    public void setPrelimRoundsPlayed(int prelimRoundsPlayed) {
        this.prelimRoundsPlayed = prelimRoundsPlayed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return id == room.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public void clearUsers() {
        this.users.clear();
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
