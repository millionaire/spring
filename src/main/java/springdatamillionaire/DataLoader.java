package springdatamillionaire;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import springdatamillionaire.entity.*;
import springdatamillionaire.repo.*;
import springdatamillionaire.service.QuestionService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import springdatamillionaire.service.RoomService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@Component
public class DataLoader implements ApplicationRunner {

    private QuestionRepository questionRepository;
    private AnswerRepository answerRepository;
    private CategoryRepository categoryRepository;
    private QuestionTypeRepository questionTypeRepository;
    private RoomRepository roomRepository;
    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;
    private QuestionService questionService;
    private RoomService roomService;
    private VoiceActionRepository voiceActionRepository;
    private VoicePhraseRepository voicePhraseRepository;

    @Autowired
    public DataLoader(
            UserRepository         userRepository,
            QuestionRepository     questionRepository,
            AnswerRepository       answerRepository,
            CategoryRepository     categoryRepository,
            QuestionTypeRepository questionTypeRepository,
            RoomRepository         roomRepository,
            UserRoleRepository     userRoleRepository,
            QuestionService        questionService,
            RoomService            roomService,
            VoiceActionRepository  voiceActionRepository,
            VoicePhraseRepository voicePhraseRepository) {
        this.questionRepository     = questionRepository;
        this.answerRepository       = answerRepository;
        this.categoryRepository     = categoryRepository;
        this.questionTypeRepository = questionTypeRepository;
        this.roomRepository         = roomRepository;
        this.userRepository         = userRepository;
        this.userRoleRepository     = userRoleRepository;
        this.questionService        = questionService;
        this.roomService            = roomService;
        this.voiceActionRepository  = voiceActionRepository;
        this.voicePhraseRepository  = voicePhraseRepository;
    }


    public void run(ApplicationArguments args) {
        if ( questionRepository.count() == 0) {
            genQuestion(0,"Сколько суток составляют високосный год?",
                    "364",false,
                    "365",false,
                    "366",true,
                    "367",false);

            genQuestion(1,"Какого цвета нет в радуге?",
                    "Красного",false,
                    "Оранжевого",false,
                    "Коричневого",true,
                    "Зеленого",false);

            genQuestion(2,"У кого из военных голубые береты?",
                    "Моряки",false,
                    "Летчики",false,
                    "Танкисты",false,
                    "Десантники",true);

            genQuestion(3,"Как называется в геометрии линия, делящая угол пополам?",
                    "Секущая",false,
                    "Биссектриса",true,
                    "Гипотенуза",false,
                    "Синусоид",false);

            genQuestion(4,"Как звали пушкинского Онегина?",
                    "Евгений",true,
                    "Александр",false,
                    "Иван",false,
                    "Михаил",false);
            genQuestion(5, "Какая бывает лопата?",
                    "Cовковая",true,
                    "Граблевая",false,
                    "Тяпковая",false,
                    "Мотыжная",false);
            genQuestion(6,"На каком курсе школы Хогвартс учился Гарри Поттер, когда раскрыл секрет Тайной комнаты?",
                    "На первом",false,
                    "На втором",true,
                    "На третьем",false,
                    "На четвёртом",false);
            genQuestion(7,"С кем воевал заглавный герой «Слова о полку Игореве»?",
                    "С половцами",true,
                    "С сарматами",false,
                    "Со сарматами",false,
                    "С печенегами",false);

            genQuestion(8,". День рождение какого персонажа в Риге отмечают на улице Яуниела?",
                    "Джона Сильвера",false,
                    "Шерлока Холмса",true,
                    "Глеба Жиглова",false,
                    "Графа Дракула",false);
            genQuestion(9,"Какая буква исчезла из русского алфавита в результате реформы Петра I?",
                    "Фита",false,
                    "Ять",false,
                    "Омега",true,
                    "'И' десятеричное",false);
            genQuestion(10, "С каким противником римляне вели Пунические войны?",
                    "со Спартой",false,
                    "с Карфагеном",true,
                    "с Египтом",false,
                    "с Македонией",false);
            genQuestion(11, "Какой учёный ввёл в науку понятие «электрический ток»?",
                    "Георг Ом",false,
                    "Луиджи Гальвани",false,
                    "Андре-Мари Ампер",true,
                    "Алессандро Вольта",false);
            genQuestion(12, "Чем занимались народные дома в дореволюционной России?",
                    "рассмотрением жалоб крестьян",false,
                    "закупкой зерна",false,
                    "здравоохранением",false,
                    "просветительством",true);
            genQuestion(13, "Что стоматолог назовет третьим моляром?",
                    "самый тонкий из буров",false,
                    "зуб мудрости",true,
                    "прочность пломбы",false,
                    "стадию кариеса",false);
            genQuestion(14, "Какая бывает лопата?",
                    "носовые",false,
                    "кормовые",false,
                    "вахтенные",false,
                    "рулевые",true);

            questionService.genPreliminaryQuestion("Расположите \"маленькие трагедии\" Пушкина в порядке написания",
                    Arrays.asList("\"Скупой рыцарь\"","\"Моцарт и Сальери\"",
                    "\"Каменный гость\"","\"Пир во время чумы\""));



         
            UserRole administrator = new UserRole("admin");
            userRoleRepository.save(administrator);
            User admin = new User(
            "admin",
                    administrator,
            BCrypt.hashpw("admin", BCrypt.gensalt())
            );
            userRepository.save(admin);


            UserRole ur = new UserRole("user");
            userRoleRepository.save(ur);
            User user = new User(
                    "vasek",
                    ur,
                    "$2a$10$ilNB1s8JI6HCmXBMaIxtfuZLcdNiZ0stQGb/oOXmgl5MzBBUZYm.e");
            System.out.println("))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))");
            System.out.println(user);
            userRepository.save(user);
            if ( roomRepository.count() == 0) {
                Room r = new Room(user, null, true, DateTime.now().plusMinutes(1).getMillis(), "");
                user.setRoom(r);
                roomRepository.save(r);
                roomService.saveToRoomsQuestion(r.getId(), questionService.randomPreliminaryQuestion(r.getId()));

            }
            UserRole banned = new UserRole("banned");
            userRoleRepository.save(banned);

            UserRole temp = new UserRole("temp");
            userRoleRepository.save(temp);

            UserRole mailunconfirmed = new UserRole("mailunconfirmed");
            userRoleRepository.save(temp);

            initPhrases2Actions();

        }


    }

    private void initPhrases2Actions() {

        // stackoverflow.com/q/2041778
        // ugly, shitty, verbose AF,  java code:


        VoiceAction va0 = new VoiceAction("продолжить игру");
        Stream.of(
            "дальше",
            "правильно",
            "неправильно",
            "следующий вопрос"
        ).forEach(va0::addPhrase);

        voiceActionRepository.save(va0);


        VoiceAction va1 = new VoiceAction("1 ответ");
        Stream.of(
            "1 ответ"
        ).forEach(va1::addPhrase);
        voiceActionRepository.save(va1);

        VoiceAction va2 = new VoiceAction("2 ответ");
        Stream.of(
            "2 ответ"
        ).forEach(va2::addPhrase);
        voiceActionRepository.save(va2);


        VoiceAction va3 = new VoiceAction("3 ответ");
        Stream.of(
            "3 ответ"
        ).forEach(va3::addPhrase);
        voiceActionRepository.save(va3);

        VoiceAction va4 = new VoiceAction("4 ответ");
        Stream.of(
            "4 ответ"
        ).forEach(va4::addPhrase);
        voiceActionRepository.save(va4);

        VoiceAction va5 = new VoiceAction("подсказка 50 на 50");
        Stream.of(
            "подсказка 50 на 50"
        ).forEach(va5::addPhrase);
        voiceActionRepository.save(va5);

        VoiceAction va6 = new VoiceAction("подсказка помощь зала");
        Stream.of(
            "подсказка помощь зала"
        ).forEach(va6::addPhrase);
        voiceActionRepository.save(va6);

        VoiceAction va7 = new VoiceAction("подсказка звонок другу");
        Stream.of(
            "подсказка звонок другу"
        ).forEach(va7::addPhrase);
        voiceActionRepository.save(va7);
    }

    private void genQuestion(int qnum,
                             String qtext,
                             String a1text, boolean a1true,
                             String a2text, boolean a2true,
                             String a3text, boolean a3true,
                             String a4text, boolean a4true) {

        Category c = new Category("Category " + qnum, qnum);
        categoryRepository.save(c);
        QuestionType qt;
        if ( questionTypeRepository.count() == 0) {
            qt = new QuestionType(QuestionType.Kind.GAME);
            questionTypeRepository.save(qt);
        }
        else {
            qt = questionTypeRepository.findAll().iterator().next();
        }
        Question q = new Question(qtext, false, c,qt);
        questionRepository.save(q);
        Answer a1 = new Answer(a1text,a1true,q);
        Answer a2 = new Answer(a2text,a2true,q);
        Answer a3 = new Answer(a3text,a3true,q);
        Answer a4 = new Answer(a4text,a4true,q);
        answerRepository.save(a1);
        answerRepository.save(a2);
        answerRepository.save(a3);
        answerRepository.save(a4);

    }
}
