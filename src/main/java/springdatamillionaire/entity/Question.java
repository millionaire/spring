package springdatamillionaire.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class Question {
    public Question(String text, boolean removed, Category category, QuestionType questionType) {
        this.text = text;
        this.removed = removed;
        this.category = category;
        this.questionType = questionType;
    }

    public Question() {
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    // public Set<Room> getRooms() {
    //     return rooms;
    // }

    // public void setRooms(Set<Room> rooms) {
    //     this.rooms = rooms;
    // }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String text;

//    @OneToMany(cascade=CascadeType.REMOVE)
//    private Set<Answer> answers;
    private boolean removed;
    @ManyToOne
    private Category category;

    @ManyToOne
    private QuestionType questionType;

    // @ManyToMany(mappedBy = "questions",fetch = FetchType.EAGER)
    // private Set<Room> rooms;

    // public void addRoom(Room room) {
    //     addRoom(room,true);
    // }

    // void addRoom(Room room,boolean add) {
    //     if (room != null) {
    //         rooms.add(room);
    //     }
        //if (add) {
        //    room.addQuestion(this,false);
        //}
    // }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
