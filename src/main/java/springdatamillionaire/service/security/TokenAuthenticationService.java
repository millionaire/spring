package springdatamillionaire.service.security;

import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import springdatamillionaire.entity.User;
import springdatamillionaire.repo.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class TokenAuthenticationService {

    private final TokenHandler tokenHandler;
    private UserRepository userRepository;

    @Autowired
    public TokenAuthenticationService(TokenHandler tokenHandler, UserRepository userRepository) {
        this.tokenHandler = tokenHandler;
        this.userRepository = userRepository;
    }

    public void addAuthentication(HttpServletResponse response, SessionUser user) {
        response.addHeader("access_token", tokenHandler.createAccessToken(user));
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        String accessToken = request.getHeader("access_token");

        if (accessToken != null) {
            try {
                System.out.println(accessToken);
                SessionUser sessionUser = TokenHandler.parseSessionUser(accessToken);
                long id = Long.parseLong(sessionUser.getUserId());
                User user = userRepository.findOne(id);
                if (user != null) {
                    sessionUser.addAuthority(new SimpleGrantedAuthority(user.getUserRole().getName()));
                }
                return new UserAuthentication(sessionUser);
            } catch (JwtException ex) {
                System.out.println("Someone tried to access with wrong token");
                return null;
            }
        }
        return null;
    }

    //can be used to update expired tokens
//    private Authentication updateToken(Claims claims, HttpServletResponse response) {
//        try {
//            SessionUser user = databaseUserDetailsService.getUser(claims.getSubject());
//            addAuthentication(response, user);
//            return new UserAuthentication(user);
//        } catch (JwtException e) {
//            return null;
//        }
//    }
}
