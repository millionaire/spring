package springdatamillionaire.DTO;

public class WSWrapper {
    private String wsMessage;
    private Object payload;

    public WSWrapper(String wsMessage, Object payload) {
        this.wsMessage = wsMessage;
        this.payload = payload;
    }

    public String getWsMessage() {
        return wsMessage;
    }

    public void setWsMessage(String wsMessage) {
        this.wsMessage = wsMessage;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
