package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.Category;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    public List<Category> findByOrd(int ord);
}
