package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.AnswerOrder;
import springdatamillionaire.entity.Category;

import java.util.List;

public interface AnswerOrderRepository extends CrudRepository<AnswerOrder, Long> {
    public List<AnswerOrder> findByAnswer_Question_Id(long id);

}