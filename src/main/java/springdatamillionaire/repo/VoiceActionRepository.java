package springdatamillionaire.repo;

import org.springframework.data.repository.CrudRepository;
import springdatamillionaire.entity.VoiceAction;

import java.util.List;

public interface VoiceActionRepository extends CrudRepository<VoiceAction, Long> {
    List<VoiceAction> findAll();
    VoiceAction findByName(String name);
}
