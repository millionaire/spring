package springdatamillionaire.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import springdatamillionaire.DTO.CategoryDTO;
import springdatamillionaire.DTO.QuestionDTO;
import springdatamillionaire.entity.*;
import springdatamillionaire.repo.*;

import java.util.*;


@Service
public class QuestionService {

    private QuestionRepository questionRepository;
    private AnswerService answerService;
    private QuestionTypeRepository questionTypeRepository;
    private CategoryRepository categoryRepository;
    private AnswerRepository answerRepository;
    private AnswerOrderRepository answerOrderRepository;
    private RoomRepository roomRepository;
    private SavedAnswerService savedAnswerService;
    private Random random;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, AnswerService answerService,
                           QuestionTypeRepository questionTypeRepository, CategoryRepository categoryRepository,
                           AnswerRepository answerRepository, AnswerOrderRepository answerOrderRepository, RoomRepository roomRepository, SavedAnswerService savedAnswerService) {
        this.questionRepository = questionRepository;
        this.answerService = answerService;
        this.questionTypeRepository = questionTypeRepository;
        this.categoryRepository = categoryRepository;
        this.answerRepository = answerRepository;
        this.answerOrderRepository = answerOrderRepository;
        this.roomRepository = roomRepository;
        this.savedAnswerService = savedAnswerService;
        this.random = new Random();
    }

    // public QuestionDTO getQuestionDTO(long id, int ord) {
    //     Question q = questionRepository.findByRooms_IdAndCategory_Ord(id, ord);
    //     return toDTO(q);
    // }

    // public Question getQuestion(long id, int ord) {
    //     return questionRepository.findByRooms_IdAndCategory_Ord(id, ord);
    // }

    public static class AnswerTuple {
        private String text;
        private Boolean isTrue;

        public AnswerTuple(String text, Boolean isTrue) {
            this.text = text;
            this.isTrue = isTrue;
        }

        String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Boolean getTrue() {
            return isTrue;
        }

        public void setTrue(Boolean aTrue) {
            isTrue = aTrue;
        }
    }

    private QuestionDTO toDTO(Question q) {
        return new QuestionDTO(q.getId(), q.getText(), answerService.findAnswersByQuestionId(q.getId()),
                q.getQuestionType().getKind() == QuestionType.Kind.PRELIMINARY, q.getCategory().getOrd());
    }

    public Question randomPreliminaryQuestion(long roomId) {
        List<Question> questions = questionRepository.findByCategory_OrdAndRemoved(-1,false);
        List<SavedAnswer> savedAnswers = savedAnswerService.getRoomSavedAnswer(roomId);
        for (SavedAnswer s : savedAnswers){
            if(questions.size() > 1){
                questions.remove(s.getQuestion());
            }
        }
        return questions.get(random.nextInt(questions.size()));

    }

    public Question randomQuestion(int ord) {
        List<Question> questions = questionRepository.findByCategory_OrdAndRemoved(ord,false);
        return questions.get(random.nextInt(questions.size()));
    }



    public void genQuestion(int qnum,
                            String qtext,
                            List<AnswerTuple> answers) {
        if (qnum == -1){
            List<String> answerTexts = new ArrayList<>();
            for (AnswerTuple answerTuple: answers){
                answerTexts.add(answerTuple.getText());
            }
            genPreliminaryQuestion(qtext,answerTexts);
            return;
        }
        List<Answer> answerList = new ArrayList<>();

        Category c = getCategory(qnum);
        QuestionType qt = getQuestionType(QuestionType.Kind.GAME);
        Question q = new Question(qtext, false, c, qt);
        questionRepository.save(q);
        answers.forEach((a -> answerList.add(new Answer(a.getText(),a.isTrue,q))));
        shuffleList(answerList);
        for (Answer a : answerList) {
            answerRepository.save(a);
        }
    }

    public void genPreliminaryQuestion(String qtext,
                                       List<String> answers) {

        Category c = getCategory(-1);
        QuestionType qt = getQuestionType(QuestionType.Kind.PRELIMINARY);
        Question q = new Question(qtext, false, c, qt);
        List<Answer> answerList = new ArrayList<>();
        List<AnswerOrder> answerOrderList = new ArrayList<>();
        questionRepository.save(q);
        for (int i = 0; i < answers.size(); i++) {
            Answer answer = new Answer(answers.get(i),true,q);
            answerList.add(answer);
            answerOrderList.add(new AnswerOrder(answer, i + 1));
        }
        shuffleList(answerList);
        for (Answer ans: answerList) {
            answerRepository.save(ans);
        }
        for (AnswerOrder ao: answerOrderList){
            answerOrderRepository.save(ao);
        }
    }

    private QuestionType getQuestionType(QuestionType.Kind questionType) {
        QuestionType qt;
        List<QuestionType> questionTypes = questionTypeRepository.findByKind(questionType.name());
        if (questionTypes.size() > 0) {
            qt = questionTypes.get(0);
        } else {
            qt = new QuestionType(questionType);
            questionTypeRepository.save(qt);
        }
        return qt;
    }

    private Category getCategory(int qnum) {
        Category c;
        List<Category> categories = categoryRepository.findByOrd(qnum);
        if (categories.size() > 0) {
            c = categories.get(0);
        } else {
            c = new Category("Category " + qnum, qnum);
            categoryRepository.save(c);
        }
        return c; 
    }

    public List<QuestionDTO> findall() {
        Iterable<Question> questions = questionRepository.findByRemoved(false);
        ArrayList<QuestionDTO> questionDTOS = new ArrayList<>();
        for (Question question : questions) {
            questionDTOS.add(toDTO(question));
        }
        return questionDTOS;
    }

    private CategoryDTO toCategoryDTO(Question q) {
        return new CategoryDTO(q.getCategory().getId(), q.getCategory().getName(), q.getCategory().getOrd());
    }

    public List<CategoryDTO> findallCategoryes() {
        Iterable<Question> questions = questionRepository.findByRemoved(false);
        ArrayList<CategoryDTO> categotyesDTOS = new ArrayList<>();
        for (Question question : questions) {
            categotyesDTOS.add(toCategoryDTO(question));
        }
        return categotyesDTOS;
    }

    public void delete(long questionId) {
        Question question = questionRepository.findOne(questionId);
        question.setRemoved(true);
        questionRepository.save(question);
    }

    public void setQuestion(long questionId, int ord,
                            String qtext,
                            List<String> answers) {
        Question question = questionRepository.findOne(questionId);
        Category c = question.getCategory();
        c.setOrd(ord);
        categoryRepository.save(c);
        question.setCategory(c);
        question.setText(qtext);
        questionRepository.save(question);
        List<Answer> answers1 = answerRepository.findByQuestion_Id(questionId);
        for (int i = 0; i < answers1.size(); i++) {
            answers1.get(i).setText(answers.get(i));
            answerRepository.save(answers1.get(i));
        }

    }

    @Scheduled(cron = "0 0 0 * * *")
    private void questionCleanup(){

        List<Question> questionsToDelete = new ArrayList<>();
        questionRepository.findAll().forEach(question -> {
            if (question.isRemoved()){
                answerRepository.delete(answerRepository.findByQuestion_Id(question.getId()));
                questionsToDelete.add(question);
            }
        });
        questionRepository.delete(questionsToDelete);
    }

    public Question findById(long questionId) {
        return questionRepository.findOne(questionId);
    }

    private static void shuffleList(List a) {
        int n = a.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(a, i, change);
        }
    }

    private static void swap(List a, int i, int change) {
        Object helper = a.get(i);
        a.set(i, a.get(change));
        a.set(change, helper);
    }

}
