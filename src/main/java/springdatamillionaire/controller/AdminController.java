package springdatamillionaire.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/**")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AdminController {

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    String users() {
        return "secret, admin";
    }
}
