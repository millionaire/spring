package springdatamillionaire.DTO;

import java.util.List;

public class RoomDTO
{
    private long id;
    private int category;
    private long activePlayer;
    private long host;
    private boolean active;
    private boolean used5050;
    private boolean usedpoll;
    private boolean usedcall;
    private List<UserDTO> users;
    private boolean continuePrelim;
    private boolean continueGame;
    private boolean anotherPlayer;
    private boolean finalAnswer;
    private boolean skipLetsplay;
    private boolean startHintCall;
    private String inviteUrl;

    public String getInviteUrl() {
        return inviteUrl;
    }

    public void setInviteUrl(String inviteUrl) {
        this.inviteUrl = inviteUrl;
    }


    public RoomDTO(
            long id,
            int category,
            long activePlayer,
            long host,
            List<UserDTO> users,
            boolean active) {
        this.id = id;
        this.category = category;
        this.activePlayer = activePlayer;
        this.host = host;
        this.users = users;
        this.active = active;
    }

    public RoomDTO(long id, int category, long activePlayer, long host, boolean active, String inviteUrl, boolean used5050, boolean usedpoll, boolean usedcall) {
        this.id = id;
        this.category = category;
        this.activePlayer = activePlayer;
        this.host = host;
        this.active = active;
        this.inviteUrl = inviteUrl;
        this.used5050 = used5050;
        this.usedpoll = usedpoll;
        this.usedcall = usedcall;
    }

    public RoomDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public long getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(long activePlayer) {
        this.activePlayer = activePlayer;
    }

    public long getHost() {
        return host;
    }

    public void setHost(long host) {
        this.host = host;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isContinuePrelim() {
        return continuePrelim;
    }

    public void setContinuePrelim(boolean continuePrelim) {
        this.continuePrelim = continuePrelim;
    }

    public boolean isContinueGame() {
        return continueGame;
    }

    public void setContinueGame(boolean continueGame) {
        this.continueGame = continueGame;
    }

    public boolean isAnotherPlayer() {
        return anotherPlayer;
    }

    public void setAnotherPlayer(boolean anotherPlayer) {
        this.anotherPlayer = anotherPlayer;
    }

    public boolean isFinalAnswer() {
        return finalAnswer;
    }

    public void setFinalAnswer(boolean finalAnswer) {
        this.finalAnswer = finalAnswer;
    }

    public boolean isSkipLetsplay() {
        return skipLetsplay;
    }

    public void setSkipLetsplay(boolean skipLetsplay) {
        this.skipLetsplay = skipLetsplay;
    }

    public boolean isUsed5050() {
        return used5050;
    }

    public void setUsed5050(boolean used5050) {
        this.used5050 = used5050;
    }

    public boolean isUsedpoll() {
        return usedpoll;
    }

    public void setUsedpoll(boolean usedpoll) {
        this.usedpoll = usedpoll;
    }

    public boolean isUsedcall() {
        return usedcall;
    }

    public void setUsedcall(boolean usedcall) {
        this.usedcall = usedcall;
    }

    public boolean isStartHintCall() {
        return startHintCall;
    }

    public void setStartHintCall(boolean startHintCall) {
        this.startHintCall = startHintCall;
    }
}
